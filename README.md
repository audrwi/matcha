# Matcha project

## By aboucher @ 42

This is a responsive dating site in the Pokemon theme.
Student project for 42 school.

### Languages

* PHP (PDO and POO)
* Javascript (jQuery library and Ajax)
* Socket.io (installed by Node)
* Elephant.io (installed by Composer)
* JSON (for the database structure and default content)
* HTML5 and Sass
* Bash (to install the Pokedate command line)
* Python (to execute the Pokedate command line)

### Applications used

* Photoshop and illustrator to create the design
* Atom to create and edit files
* Prepros to compile Sass to CSS
* iTerm2 to start the server and use git

### Get started

Git clone and execute the bash file to install Pokedate.
You need to use 'source' or add ~/.matcha/pokedate to your environment PATH to use it.

```
cd path-to-project
source ./install.sh
```

Then, you can execute the Pokedate command line to start the server :

```
pokedate server:start
```

Warning: You need PAMP and MySQL (installed with PAMP) to start it.
You can also clear the database :

```
pokedate db:clear
```

All informations you need are available by typing :

```
pokedate --help
```


Now you can use the application by typing the following address in your browser :

```
localhost:8888
```

Some fake account has been now created.
You can find the list of them in [config/db/data_users.json](config/db/data_users.json).
They all have the password : Fakeaccount33

### Admin accounts

* audrwi
* admin - *password: admin*

### Bonus

* Python launcher
* URL rewriting (with the router file : [config/router.php](config/router.php))
* Cropper in profile page (to resize your cover in the good sizes)
* Possibility to change the theme when you're connected
* Possibility to use emojis in the chat
* Possibility to dislike a user's profile
* Admin page to see users reported by others and delete them
* HTML emailings
* Design
