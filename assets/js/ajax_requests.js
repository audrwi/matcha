// CHECK FORMS

function completedRegisterForm(elem) {
	var valid	= true;
	$('#success, #error').fadeOut();
	$('form > div').each(function() {
		var that = $(this);
		if (isEmpty($(this))) {
			valid = false;
			if ($(this).find('input').attr('name') === 'password')
				resetInput($(this));
			if (elem === null || $(this).html() === elem.html()) {
				removeInfos($(this), 'success');
				addInteraction($(this), 'error', 'Please fill this information');
			} else if (elem !== null)
				resetInput($(this));
			$('form input[type=submit]').prop('disabled', !valid);
		} else if ($(this).closest('form').prop('id') === 'new_password' && $(this).find('input').attr('name') === 'email') {
			resetInput($(this));
			$(this).addClass('disabled').find('input').attr('disabled', 'disabled').val(getUrlVars()[2]);
		} else if ($(this).find('input').attr('name') === 'confirm_password') {
			if ($('input[name=password]').val() === $('input[name=confirm_password]').val() && $('input[name=confirm_password]').val() !== "") {
				removeInfos($(this), 'error');
				addInteraction($(this), 'success', null);
			} else {
				valid = false;
				removeInfos(that, 'success');
				addInteraction(that, 'error', 'Wrong confirmation');
			}
		} else {
			$.ajax({
				url: '_checkRegisterForm.php',
				method: 'POST',
				data: {
					name:	$(this).find('input').attr('name'),
					value:	$(this).find('input' + ((containInput($(this), 'radio') || containInput($(this), 'checkbox')) ? ':checked' : '')).val()
				}
			}).done(function(r) {
				if (r === 'error') {
					valid = false;
					removeInfos(that, 'success');
					addInteraction(that, 'error', 'Invalid information');
				} else if (r === 'low' || r === 'middle' || r === 'high') {
					resetInput(that);
					valid = (r !== 'high') ? false : valid;
					that.addClass(r);
				} else if (r.length && r.length < 30) {
					valid = false;
					removeInfos(that, 'success');
					addInteraction(that, 'error', r);
				} else if (r.length) {
				} else {
					removeInfos(that, 'error');
					addInteraction(that, 'success', null);
				}
				$('form input[type=submit]').prop('disabled', !valid);
			});
		}
	});
}

function completedProfileForm(elem) {
	var valid	= true;
	$('#success, #error').fadeOut();
	$('input[name=centers_of_interest]').parent().next('ul').fadeOut();
	$('.profile form > div:not(.tags, .covers)').each(function() {
		var that = $(this);
		if ($(this).find('input').attr('name') === 'centers_of_interest' && $('input[name=centers_of_interest]').val() !== '') {
			$.ajax({
				url: '_newTag.php',
				method: 'POST',
				data: {
					token:	$('input[name=token]').val(),
					tag:	$('input[name=centers_of_interest]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (!r.length) {
					resetInput($('input[name=centers_of_interest]').parent('div'));
					if ($('div.tags > span').length)
						$('div.tags > span:last-child').after('<span>#' + $('input[name=centers_of_interest]').val() + '<span class="icon-cross"></span></span>');
					else
						$('div.tags').append('<span>#' + $('input[name=centers_of_interest]').val() + '<span class="icon-cross"></span></span>');
					$('input[name=centers_of_interest]').val("");
					$('#ui-id-1').fadeOut();
				} else {
					valid = false;
					addInteraction($('input[name=centers_of_interest]').parent('div'), 'error', r);
				}
			});
		} else if (isEmpty($(this))) {
			if (!changePassword($(this)) && $(this).find('input').attr('name') !== 'centers_of_interest')
				valid = false;
			if ($(this).find('input').attr('name') === 'password')
				resetInput($(this));
			if (elem !== null && $(this).html() === elem.html()) {
				removeInfos($(this), 'success');
				addInteraction($(this), 'error', 'Please fill this information');
			} else if (elem !== null)
				resetInput($(this));
		} else if ($(this).find('input').attr('name') === 'confirm_password') {
			if ($('input[name=password]').val() === $('input[name=confirm_password]').val() && $('input[name=confirm_password]').val() !== "") {
				removeInfos($(this), 'error');
				addInteraction($(this), 'success', null);
			} else {
				valid = false;
				removeInfos(that, 'success');
				addInteraction(that, 'error', 'Wrong confirmation');
			}
		} else {
			$.ajax({
				url: '_checkProfileForm.php',
				method: 'POST',
				data: {
					token:	$('input[name=token]').val(),
					name:	$(this).find('input, select, textarea').attr('name'),
					value:	$(this).find('input' + ((containInput($(this), 'radio') || containInput($(this), 'checkbox')) ? ':checked' : '') + ', select, textarea').val()
				}
			}).done(function(r) {
				if (r === 'error') {
					valid = false;
					removeInfos(that, 'success');
					addInteraction(that, 'error', 'Invalid information');
				} else if (r === 'error_token') {
					removeInfos(that, 'success');
					addInteraction(that, 'error', 'Something went wrong');
				} else if (r === 'low' || r === 'middle' || r === 'high') {
					resetInput(that);
					valid = (r !== 'high') ? false : valid;
					that.addClass(r);
				} else if (r === 'same') {
					resetInput(that);
				} else if (r !== 'same' && that.find('input').attr('name') === 'old_password' && elem === null) {
					valid = false;
					removeInfos(that, 'success');
					addInteraction(that, 'error', 'Wrong password');
				} else if (r.length && r.length < 30) {
					valid = false;
					removeInfos(that, 'success');
					addInteraction(that, 'error', r);
				} else if (r.length) {
				} else if (that.find('input').attr('name') === 'old_password') {
					removeInfos(that, 'error');
				} else if (elem !== null && that.html() === elem.html()) {
					removeInfos(that, 'error');
					addInteraction(that, 'success', null);
				}
			});
		}
	});
	$.ajax({
		url: '_checkCoversAndTags.php',
		method: 'POST'
	}).done(function(r) {
		if (r === 'false')
			valid = false;
		if (valid === true)
			$('button#update, .actions a').removeClass('disabled');
		else
			$('button#update, .actions a').addClass('disabled');
	});
}

$(function (){

	// REGISTER

	$('form#register').submit(function(e) {
		e.preventDefault();
		completedRegisterForm(null);
		$('#success, #error').fadeOut();
		if ($(this).children('input[type=submit]').prop('disabled') === false) {
			$.ajax({
				url: '_register.php',
				method: 'POST',
				data: {
					first_name:			$('input[name=first_name]').val(),
					last_name:			$('input[name=last_name]').val(),
					email:				$('input[name=email]').val(),
					gender:				$('input[name=gender]:checked').val(),
					username:			$('input[name=username]').val(),
					password:			$('input[name=password]').val(),
					confirm_password:	$('input[name=confirm_password]').val(),
					birth_date:			$('input[name=birth_date]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length)
					$('#error').fadeIn().html(r);
				else {
					$('input[name=first_name], input[name=last_name], input[name=email], input[name=username], input[name=password], input[name=confirm_password], input[name=birth_date]').val("");
					resetInput($('input[name=first_name], input[name=last_name], input[name=email], input[name=username], input[name=password], input[name=confirm_password], input[name=birth_date], input[name=gender]').closest('div'));
					$('input[name=gender][value=female]').prop('checked', 'checked').parent().siblings('.radio').children('input').removeAttr('checked');
					$('form input[type=submit]').prop('disabled', true);
					$('#success').fadeIn();
				}
			});
		}
	});

	// FORGOT PASSWORD

	$('form#forgot').submit(function(e) {
		e.preventDefault();
		$('#success, #error').fadeOut();
		if (isEmpty($('form > div'))) {
			removeInfos($(this), 'success');
			addInteraction($(this), 'error', 'Please fill this information');
		} else {
			$.ajax({
				url: '_forgot.php',
				method: 'POST',
				data: {
					email:	$('input[name=email]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length) {
					addInteraction($('form > div'), 'error', r);
				} else {
					$('input[name=email]').val("");
					resetInput($('input[name=email]').closest('div'));
					$('form input[type=submit]').prop('disabled', true);
					$('#success').fadeIn();
				}
			});
		}
	});

	$('#new_password').submit(function(e) {
		e.preventDefault();
		$('#success, #error').fadeOut();
		completedRegisterForm(null);
		if ($(this).children('input[type=submit]').prop('disabled') === false) {
			$.ajax({
				url: '_newPassword.php',
				method: 'POST',
				data: {
					email:				getUrlVars()[2],
					token:				getUrlVars()[1],
					password:			$('input[name=password]').val(),
					confirm_password:	$('input[name=confirm_password]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length)
					$('#error').fadeIn().html(r);
				else {
					var seconds = 5;
					$('input[name=password], input[name=confirm_password]').val("");
					resetInput($('input[name=password], input[name=confirm_password]').closest('div'));
					$('form input[type=submit]').prop('disabled', true);
					$('#success').fadeIn().find('span').text(seconds);
					var interval = setInterval(function() {
						if (seconds > 0) {
							seconds--;
							$('#success span').text(seconds);
						} else {
							clearInterval(interval);
							window.location.href = '/login';
						}
					}, 1000);
				}
			});
		}
	});

	// LOGIN

	$('#login').submit(function(e) {
		e.preventDefault();
		$('#error').fadeOut();
		var valid = true;
		$('form > div').each(function() {
			if (isEmpty($(this))) {
				valid = false;
				addInteraction($(this), 'error', "Fill this information");
				$('form input[type=submit]').prop('disabled', true);
			} else
				resetInput($(this));
		});
		if (valid === true) {
			$('form input[type=submit]').prop('disabled', false);
			$.ajax({
				url: '_login.php',
				method: 'POST',
				data: {
					user:		$('input[name=user]').val(),
					password:	$('input[name=password]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length)
					$('#error').fadeIn().html(r);
				else
					window.location.href = '/';
			});
		}
	});

	// COVER

	$('.cropper button[name=new_cover]').click(function() {
		if ($('.cropper img').length === 1 && $('.cropper img').attr('src') !== null && $('.cropper img').attr('src') !== '') {
			if (!$(this).hasClass('disabled')) {
				$('.cropper p.error').text('').fadeOut();
				var div		= $('.cropper .size').offset(),
					img		= $('.cropper img').offset();
				$.ajax({
					url: '_newCover.php',
					method: 'POST',
					data: {
						token:	$('input[name=token]').val(),
						image:		$('.cropper img').attr('src'),
						width:		$('.cropper img').width(),
						height:		$('.cropper img').height(),
						x:			(div.left - img.left),
						y:			(div.top - img.top),
						scale:		$('.cropper img').prop('naturalWidth') / $('.cropper img').width()
					},
					beforeSend: function() {
						$('div.background').css({
							'z-index':	120
						});
						loader();
					}
				}).done(function(r) {
					$('div.background').css({
						'z-index':	25
					});
					$('.loader').fadeOut();
					var regex = /[a-z0-9]+.jpg/;
					if (r.length && regex.test(r)) {
						$('div.background, .cropper').fadeOut();
						cropper = false;
						$('input[type=file]').val("");
						$('#text-content').text("Choose an image...");
						$('.cropper img').attr('src', "").removeAttr('style');
						$('.cropper figure').removeClass('active');
						$('.cropper button').addClass('disabled');
						$('.cropper p.error').text('').fadeOut();
						$('body').removeClass('noscroll');
						if ($('.covers > div:not(.cropper).empty').length === 5)
							$('.covers > div:not(.cropper):first').addClass('first');
						$('.covers div.empty:first').removeClass('empty').children('img').attr('src', 'assets/imgs/users/' + r);
					} else
						$('.cropper p.error').text(r).fadeIn();
				});
				completedProfileForm(null);
			}
		} else {
			$('.cropper p.error').text('No image found').fadeIn();
			$('button[name=new_cover], .actions a').addClass('disabled');
		}
	});

	$('.covers > div:not(.cropper)').click(function() {
		var $this = $(this);
		if ($(this).hasClass('empty')) {
			$('.cropper p.error').text('').fadeOut();
			$('input[type=file]').val("");
			$('#text-content').text("Choose an image...");
			$('.cropper img').attr('src', "").removeAttr('style');
			$('.cropper figure').removeClass('active');
			$('.cropper button').addClass('disabled');
			$('#success, #error').fadeOut();
			$('div.background, .cropper').fadeIn();
			$('body').addClass('noscroll');
			cropper = true;
		} else if (!$(this).hasClass('first')) {
			$.ajax({
				url: '_changeFirstCover.php',
				method: 'POST',
				data: {
					token:	$('input[name=token]').val(),
					image:	$(this).children('img').attr('src').split('assets/imgs/users/')[1]
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length) {
					$('#success').fadeOut();
					$('#error').fadeIn().html(r);
				} else {
					$this.siblings('.first').removeClass('first');
					$this.addClass('first');
				}
			});
		}
		completedProfileForm(null);
	});

	$('.covers > div:not(.cropper) .icon-cross').click(function(e) {
		e.stopPropagation();
		var $this = $(this);
		$.ajax({
			url: '_deleteCover.php',
			method: 'POST',
			data: {
				token:	$('input[name=token]').val(),
				image:	$(this).siblings('img').attr('src').split('assets/imgs/users/')[1]
			},
			beforeSend: loader()
		}).done(function(r) {
			loader_stop();
			if (r.length) {
				$('#success').fadeOut();
				$('#error').fadeIn().html(r);
			} else {
				$this.siblings('img').attr('src', '').parent('div').addClass('empty');
				if ($this.parent('div').hasClass('first')) {
					$this.parent('div').removeClass('first');
					if ($('.covers > div:not(.cropper).empty').length < 5)
						$('.covers > div:not(.cropper, .empty):first').addClass('first');
				}
			}
		});
		completedProfileForm(null);
	});

	// TAGS

	$('.profile input[name=centers_of_interest]').keyup(function(e) {
		if (e.which === 13) {
			e.preventDefault();
			$.ajax({
				url: '_newTag.php',
				method: 'POST',
				data: {
					token:	$('input[name=token]').val(),
					tag:	$('input[name=centers_of_interest]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (!r.length) {
					resetInput($('input[name=centers_of_interest]').parent('div'));
					if ($('div.tags > span').length)
						$('div.tags > span:last-child').after('<span>#' + $('input[name=centers_of_interest]').val() + '<span class="icon-cross"></span></span>');
					else
						$('div.tags').append('<span>#' + $('input[name=centers_of_interest]').val() + '<span class="icon-cross"></span></span>');
					$('input[name=centers_of_interest]').val("");
					$('#success').fadeIn().html('Tag successfully added.');
					$('#ui-id-1').fadeOut();
				} else {
					valid = false;
					addInteraction($('input[name=centers_of_interest]').parent('div'), 'error', r);
				}
			});
		} else {
			$('#success, #error').fadeOut();
			resetInput($('input[name=centers_of_interest]').parent('div'));
		}
	});

	$('.profile .tags').on('click', 'span span.icon-cross', function() {
		$('#success, #error').fadeOut();
		var $this = $(this);
		$.ajax({
			url: '_deleteTag.php',
			method: 'POST',
			data: {
				token:	$('input[name=token]').val(),
				tag:	$(this).parent().text().split('#')[1]
			},
			beforeSend: loader()
		}).done(function(r) {
			loader_stop();
			if (!r.length) {
				$this.parent().fadeOut().remove();
				$('#success').fadeIn().html('Tag successfully removed.');
			} else
				$('#error').fadeIn().html(r);
		});
		completedProfileForm(null);
	});

	$.ajax({
		url: '_getTags.php',
		dataType: "json",
		method: 'POST'
	}).done(function(r) {
		if (r.length)
			var centersOfInterest = r;
		if (centersOfInterest) {
			$('input[name=centers_of_interest]').autocomplete({
				source: centersOfInterest
			});
			$('#ui-id-1').insertAfter($('input[name=centers_of_interest]').parent());
		}
	});

	// PROFILE

	$('button#update').click(function(e) {
		e.preventDefault();
		completedProfileForm(null);
		$('#success, #error').fadeOut();
		if (!$('button#update').hasClass('disabled')) {
			$.ajax({
				url: '_updateProfile.php',
				method: 'POST',
				data: {
					token:				$('input[name=token]').val(),
					first_name:			$('input[name=first_name]').val(),
					last_name:			$('input[name=last_name]').val(),
					email:				$('input[name=email]').val(),
					interested_by:		$('select[name=sexual_orientation]').val(),
					birth_date:			$('input[name=birth_date]').val(),
					old_password:		$('input[name=old_password]').val(),
					password:			$('input[name=password]').val(),
					confirm_password:	$('input[name=confirm_password]').val(),
					gender:				$('input[name=gender]:checked').val(),
					pokemon_type:		$('select[name=pokemon_type]').val(),
					location:			$('input[name=location]').val(),
					description:		$('textarea[name=description]').val()
				},
				beforeSend: loader()
			}).done(function(r) {
				loader_stop();
				if (r.length)
					$('#error').fadeIn().html(r);
				else {
					resetInput($('form > div'));
					$('input[name*=password]').val("");
					$('.actions a').removeClass('disabled');
					$('#success').fadeIn().text('You have successfully updated your account.');
				}
			});
		}
	});

	// DELETE ACCOUNT

	$('.dialog.delete p + button').click(function() {
		$.ajax({
			url: '_deleteAccount.php',
			method: 'POST',
			data: {
				token:	$('input[name=token]').val()
			},
			beforeSend: loader()
		}).done(function(r) {
			loader_stop();
			if (r.length)
				$('#error').fadeIn().html(r);
			else
				window.location.href = '/logout';
		});
	});

	// BLOCK / LIKE / DISLIKE / REPORT

	$('.trainer button#block').click(function() {
		$('#success, #error').fadeOut();
		if (!$(this).hasClass('disabled')) {
			$.ajax({
				url: '_isNotInInteractionWithUser.php',
				method: 'POST',
				data: {
					token:		$('input[name=token]').val(),
					username:	getUrlVars()[1],
					action:		'block'
				}
			}).done(function(r) {
				if (r.length)
					$('#error').fadeIn().html(r);
				else
					$('div.background, .dialog.block').fadeIn();
			});
		}
	});

	$('.trainer .dialog.block p + button').click(function (){
		$('#success, #error, div.background, .dialog.block').fadeOut();
		$.ajax({
			url: '_interactWithUser.php',
			method: 'POST',
			data: {
				token:		$('input[name=token]').val(),
				username:	getUrlVars()[1],
				action:		'block'
			}
		}).done(function(r) {
			if (r.length)
				$('#error').fadeIn().html(r);
			else {
				$('.trainer p.like_state, .trainer p.dislike_state').text("");
				$('.trainer p.block_state').text('You blocked this trainer.');
				$('.trainer button#block').addClass('disabled');
				$('.trainer button#like, .trainer button#dislike, .trainer a#message').remove();
				$('#success').fadeIn().html('You successfully blocked this trainer.');
			}
		});
	});

	$('.trainer button#report').click(function() {
		$('#success, #error').fadeOut();
		if (!$(this).hasClass('disabled')) {
			$.ajax({
				url: '_isNotInInteractionWithUser.php',
				method: 'POST',
				data: {
					token:		$('input[name=token]').val(),
					username:	getUrlVars()[1],
					action:		'report'
				}
			}).done(function(r) {
				if (r.length)
					$('#error').fadeIn().html(r);
				else
					$('div.background, .dialog.report').fadeIn();
			});
		}
	});

	$('.trainer .dialog.report p + button').click(function (){
		$('#success, #error, div.background, .dialog.report').fadeOut();
		$.ajax({
			url: '_interactWithUser.php',
			method: 'POST',
			data: {
				token:		$('input[name=token]').val(),
				username:	getUrlVars()[1],
				action:		'report'
			}
		}).done(function(r) {
			if (r.length)
				$('#error').fadeIn().html(r);
			else {
				$('.trainer p.report_state').text('You reported this trainer as a fake account.');
				$('.trainer button#report').addClass('disabled');
				$('#success').fadeIn().html('You successfully reported this trainer as a fake account.');
			}
		});
	});

	$('.trainer button#like').click(function() {
		$('#success, #error').fadeOut();
		if (!$(this).hasClass('disabled')) {
			$.ajax({
				url: '_interactWithUser.php',
				method: 'POST',
				data: {
					token:		$('input[name=token]').val(),
					username:	getUrlVars()[1],
					action:		'like'
				}
			}).done(function(r) {
				if (r.length && r !== 'me' && r !== 'both' && r !== 'unlike-yes' && r !== 'unlike-no')
					$('#error').fadeIn().html(r);
				else {
					$('.trainer p.like_state').text((r === 'me') ? 'You liked this trainer.' : 'You liked each other\'s profile. Now you can talk.');
					$('.trainer button#like').addClass('unlike');
					$('.trainer button#dislike').addClass('disabled');
					$('#success').fadeIn().html('You successfully ' + ((r === 'unlike-yes' || r === 'unlike-no') ? 'un' : '') + 'liked this trainer.');
					if (r === 'both') {
						$('.trainer button#dislike').remove();
						$('.trainer button#like').addClass('unlike').after('<a href="/chat" id="message"><button class="icon-talk"></button></a>');
						$('.trainer p.like_state').text('You liked each other\'s profile. Now you can talk.');
					} else if (r === 'me') {
						$('.trainer button#like').addClass('unlike');
						$('.trainer p.like_state').text('You liked this trainer.');
					} else if (r === 'unlike-yes' || r === 'unlike-no') {
						$('.trainer button#like').removeClass('unlike');
						$('.trainer button#dislike').removeClass('disabled');
						$('.trainer a#message').remove();
						if (!$('.trainer button#dislike').length)
							$('.trainer button#like').after('<button id="dislike" class="icon-dislike"></button>');
						if (r === 'unlike-yes')
							$('.trainer p.like_state').text('This trainer liked your profile.');
						else if (r === 'unlike-no')
							$('.trainer p.like_state').text('');
					}
				}
			});
		}
	});

	$('.trainer button#dislike').click(function() {
		$('#success, #error').fadeOut();
		if (!$(this).hasClass('disabled')) {
			$.ajax({
				url: '_interactWithUser.php',
				method: 'POST',
				data: {
					token:		$('input[name=token]').val(),
					username:	getUrlVars()[1],
					action:		'dislike'
				}
			}).done(function(r) {
				if (r.length && r !== 'me' && r !== 'both')
					$('#error').fadeIn().html(r);
				else {
					$('.trainer p.dislike_state').text((r === 'me') ? 'You disliked this trainer.' : 'You disliked each other\'s profile. NO WAY !');
					$('.trainer button#like, .trainer button#dislike').addClass('disabled');
					$('#success').fadeIn().html('You successfully disliked this trainer.');
				}
			});
		}
	});

	// CHANGE THEME

	$('.colorscheme div').click(function() {
		var that	= $(this),
			theme	= $(this).attr('class');
		$('#success, #error').fadeOut();
		if (!$(this).hasClass('mine')) {
			$.ajax({
				url: '_changeColorscheme.php',
				method: 'POST',
				data: {
					theme:	$(this).attr('class')
				}
			}).done(function(r) {
				if (r.length) {
					$('.colorscheme, div.background').fadeOut();
					$('#error').fadeIn().html(r);
				} else {
					that.addClass('mine').siblings().removeClass('mine');
					$('body').attr('class', theme);
				}
			});
		}
	});

	// SEARCH PAGE

	$('.search > figure').on('click', 'span', function() {
		var num		= $(this).attr('user-id'),
			that	= $(this);
		$('#success, #error').fadeOut();
		if (num.length && Math.floor(num) == num && $.isNumeric(num) && num > 0) {
			$.ajax({
				url: '_getUser.php',
				method: 'POST',
				data: {
					id: num
				},
				dataType: 'json'
			}).done(function(r) {
				if (r) {
					if (r.cover.length && r.username.length && r.gender.length && r.connected.length && r.age.length && r.place.length && r.pokemon_type.length) {
						$('.profile_user img').attr('src', 'assets/imgs/users/' + r.cover);
						$('.profile_user h2').html(r.username + '<span class="icon-' + r.gender + '"></span>');
						$('.profile_user p.connected').removeClass('true false').addClass(r.connected).text(r.connected === 'true' ? 'Connected' : 'Disconnected');
						$('.profile_user p.age span').text(r.age);
						$('.profile_user p.type').removeClass().addClass('type ' + r.pokemon_type);
						$('.profile_user a').attr('href', 'trainer-' + r.username);
						$('.profile_user p.place span.location').text(r.place);
						$('.tuto_msg').hide();
						$('.profile_user').fadeIn();
						that.removeClass('unclicked').siblings().addClass('unclicked');
					} else if (r.length)
						$('#error').fadeIn().html(r);
				} else
					$('#error').fadeIn().html('Something went wrong.');
			});
		} else
			$('#error').fadeIn().html('Invalid information.');
	});

	function filterAndSort() {
		if ($('#content.search').length) {
			$('#error').fadeOut();
			$.ajax({
				url: '_filterAndSort.php',
				method: 'POST',
				data: {
					sortBy: $('form#sort input[type=radio]:checked').val(),
					filters: {
						place:		$('form#filter input[name=place]').is(':checked') ? true : false,
						placeVal:	$('form#filter input[name=place]').val(),
						tags:		$('form#filter input[name=tag]').is(':checked') ? true : false,
						tagsVal:	$('form#filter input[name=tag]').val(),
						type:		$('form#filter input[name=type]').is(':checked') ? true : false,
						typeVal:	$('form#filter input[name=type]').val(),
						age:		$('form#filter input[name=age]').is(':checked') ? true : false,
						ageVal:		$('form#filter input[name=age]').val(),
						pop:		$('form#filter input[name=popularity]').is(':checked') ? true : false,
						popVal:		$('form#filter input[name=popularity]').val(),
					}
				}
			}).done(function(r) {
				if (!r.length) {
					$('.tuto_msg').fadeIn();
					$('.profile_user').fadeOut();
					$('#error').fadeIn().html('Some invalid information(s).');
					$('figure.map span').remove();
				} else if (r[0].length) {
					$('.tuto_msg').fadeIn();
					$('.profile_user').fadeOut();
					var users	= JSON.parse(r),
						count;
					if (users !== null) {
						$.each(users, function(i) {
							count = i + 1;
							var content = '<span user-id="' + users[i].id + '"><figure><img src="assets/imgs/users/' + users[i].cover + '" alt="Cover"></figure><p>' + users[i].username + '</p></span>';
							if ($('figure.map span:nth-child(' + count + ')').length && $('figure.map span:nth-child(' + count + ')').attr('user-id') !== users[i].id)
								$('figure.map span:nth-child(' + count + ')').replaceWith(content);
							else if (!$('figure.map span:nth-child(' + count + ')').length)
								$('figure.map').append(content);
						});
						$('figure.map span:gt(' + (count - 1) + ')').remove();
						$('figure.map span').removeClass('unclicked');
					} else
						$('figure.map span').remove();
				}
			});
		}
	}

	$('#age_slider, #popularity_slider').slider({
		range: true,
		min: 18,
		max: 115,
		values: [18, 115],
		slide: function(e, ui) {
			$('input[name=age]').val(ui.values[0] + ' - ' + ui.values[1]);
			$('#age_slider div + span').html('<p>' + ui.values[0] + '</p>');
			$('#age_slider span + span').html('<p>' + ui.values[1] + '</p>');
			if ($('input[name=age]').prop('checked') === true)
				filterAndSort();
		}
	});

	$('#popularity_slider').slider({
		range: true,
		min: 0,
		max: 1000,
		values: [0, 1000],
		slide: function(e, ui) {
			$('input[name=popularity]').val(ui.values[0] + ' - ' + ui.values[1]);
			$('#popularity_slider div + span').html('<p>' + ui.values[0] + '</p>');
			$('#popularity_slider span + span').html('<p>' + ui.values[1] + '</p>');
			if ($('input[name=popularity]').prop('checked') === true)
				filterAndSort();
		}
	});

	$('.search .checkbox label').click(function() {
		$(this).siblings('input').prop('checked', !$(this).siblings('input').prop('checked'));
		filterAndSort();
	});

	$('.search .radio label').click(function() {
		$(this).siblings('input').prop('checked', 'checked').parent().siblings('.radio').children('input').removeAttr('checked');
		filterAndSort();
	});

	$('.search select').change(function() {
		$('input[name=type]').val($(this).val());
		if ($('input[name=type]').prop('checked') === true)
			filterAndSort();
	});

	$('.search input[name=location]').keyup(function() {
		$('input[name=place]').val($(this).val());
		if ($('input[name=place]').prop('checked') === true)
			filterAndSort();
	});

	$('.search input[name=centers_of_interest]').keyup(function(e) {
		var regex	= /^[a-z0-9]{1,15}$/i,
			that	= $(this);
		if (e.which === 13) {
			e.preventDefault();
			var tags	= $('input[name=tag]').val(),
				tab		= tags.split(';'),
				newTab	= [],
				j		= 0;
			for (var i = 0; i < tab.length; i++) {
				if (tab[i] !== '') {
					newTab[j] = tab[i];
					j++;
				}
			}
			if (that.val().length && regex.test(that.val())) {
				$('#error').fadeOut();
				if ($('div.tags > span').length !== newTab.length)
					$('#error').fadeIn().html('Something went wrong.');
				else if (newTab.length > 2)
					$('#error').fadeIn().html('Maximum 3 tags.');
				else {
					var valid = true;
					if (newTab.length) {
						$.each(newTab, function(i) {
							if (newTab[i] !== $('div.tags > span:nth-child(' + (i + 1) + ')').text().split('#')[1] || newTab[i] === that.val()) {
								$('#error').fadeIn().html('Something went wrong.');
								valid = false;
								return false;
							}
						});
					}
					if (valid) {
						$('input[name=tag]').val($('input[name=tag]').val() + that.val() + ';');
						$('div.tags').fadeIn().append('<span>#' + that.val() + '<span class="icon-cross"></span></span>');
						that.val("");
						$('.input.interest').next('ul').fadeOut();
						if ($('input[name=tag]').prop('checked') === true)
							filterAndSort();
					}
				}
			} else
				$('#error').fadeIn().html('Invalid tag.');
		}
	});

	$('.search .tags').on('click', 'span.icon-cross', function() {
		$('#error').fadeOut();
		var tags	= $('input[name=tag]').val(),
			tab		= tags.split(';'),
			newTab	= [],
			j		= 0,
			that	= $(this).parent();
		for (var i = 0; i < tab.length; i++) {
			if (tab[i] !== '') {
				newTab[j] = tab[i];
				j++;
			}
		}
		if ($('div.tags > span').length !== newTab.length)
			$('#error').fadeIn().html('Something went wrong.');
		else {
			var valid = false;
			$.each(newTab, function(i) {
				if (newTab[i] === that.text().split('#')[1]) {
					that.remove();
					if (!$('div.tags > span').length)
						$('div.tags').hide();
					$('input[name=tag]').val($('input[name=tag]').val().replace(that.text().split('#')[1], ''));
					if ($('input[name=tag]').prop('checked') === true)
						filterAndSort();
					valid = true;
					return false;
				}
			});
			if (!valid)
				$('#error').fadeIn().html('Something went wrong.');
		}
	});

	// CHAT PAGE

	var socket;

	$('.chat .list div').on('click', 'div', function() {
		var num		= $(this).attr('user-id'),
			that	= $(this);
		$('#success, #error').fadeOut();
		if (num.length && Math.floor(num) == num && $.isNumeric(num) && num > 0) {
			if (socket)
				socket.abort();
			$.ajax({
				url: '_getConversation.php',
				method: 'POST',
				data: {
					id:	num
				}
			}).done(function(r) {
				if (!r.length)
					$('#error').fadeIn().html('Something went wrong.');
				else if (r[0].length) {
					$('.talk .messages').html('');
					var messages = JSON.parse(r);
					that.addClass('onchat').siblings().removeClass('onchat');
					that.children('.info_bubble').fadeOut();
					if (!that.siblings().children('.info_bubble').length)
						$('.nav-chat').removeClass('new');
					if (messages !== null) {
						$('.talk > div p.nomessage').hide();
						$.each(messages, function(i) {
							if (i === 0)
								$('.talk .messages').html('<p class="date"><span>' + messages[i].date + '</span></p>');
							else if (messages[i - 1].date !== messages[i].date)
								$('.talk .messages').html($('.talk .messages').html() + '<p class="date"><span>' + messages[i].date + '</span></p>');
							if (messages[i].sender === 'other')
								$('.talk .messages').html($('.talk .messages').html() + '<div class="sender-' + messages[i].sender + '"><span class="hour">' + messages[i].hour + '</span><p class="message">' + messages[i].message + '</p></div>');
							else
								$('.talk .messages').html($('.talk .messages').html() + '<div class="sender-' + messages[i].sender + '"><p class="message">' + messages[i].message + '</p><span class="hour">' + messages[i].hour + '</span></div>');
						});
					} else
						$('.talk > div p.nomessage').show();
					$('.empty').fadeOut();
					$('form#send-message input[type=text]').val("");
					$('.talk a').attr('href', 'trainer-' + that.children('p').text());
					$('.talk').attr('user-id', num).show();
					$('.talk').scrollTop($('.talk .messages').innerHeight());
				}
			});
		} else
			$('#error').fadeIn().html('Something went wrong.');
	});

	$('span.icon-send').click(function() {
		$('form#send-message').submit();
	});

	$('form#send-message').submit(function(e) {
		e.preventDefault();
		var num		= $('.talk').attr('user-id'),
			that	= $(this);
		if (num.length && Math.floor(num) == num && $.isNumeric(num) && num > 0) {
			$.ajax({
				url: '_newMessage.php',
				method: 'POST',
				data: {
					token:		$('input[name=token]').val(),
					id:			num,
					message:	$('form#send-message input[type=text]').val()
				}
			}).done(function(r) {
				if (r.length === 'Something went wrong')
					$('#error').fadeIn().html(r);
				else {
					$('.talk > div p.nomessage').hide();
					$('.talk .messages').html($('.talk .messages').html() + r);
					$('form#send-message input[type=text]').val("");
					$('.talk').scrollTop($('.talk .messages').innerHeight());
				}
			});
		} else
			$('#error').fadeIn().html('Something went wrong.');
	});

	// ADMIN

	$('.admin #admin_delete button').click(function() {
		var num		= $(this).attr('user-id'),
			that	= $(this);
		$('#success, #error').fadeOut();
		if (num.length && Math.floor(num) == num && $.isNumeric(num) && num > 0) {
			$.ajax({
				url: '_adminDeleteUser.php',
				method: 'POST',
				data: {
					token:	$('input[name=token]').val(),
					id:		num
				}
			}).done(function(r) {
				if (r.length)
					$('#error').fadeIn().html(r);
				else {
					$('#success').fadeIn();
					that.parents('article').fadeOut().remove();
					if (!$('.admin .container article').length)
						$('.admin .container').append('<p>No user reported as a fake account.</p>');
				}
			});
		} else
			$('#error').fadeIn().html('Something went wrong.');
	});

});
