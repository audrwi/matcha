// VARIABLES

var cropper = false,
	old_x,
	old_y,
	selected = 0,
	timer;

// INPUT TYPE FILE CUSTOM

function customFileButton(id) {
	var input	= document.getElementById(id);
	input.addEventListener('change', function(e) {
		var name	= e.target.value.split('\\').pop(),
			img		= $('.cropper img'),
			fig		= $('.cropper figure');
		document.getElementById('text-content').innerText = (name) ? name : "Choose an image...";
		if (!name) {
			input.value = "";
			img.attr('src', "").removeAttr('style').hide();
			fig.removeClass('active');
			$('.cropper button').addClass('disabled');
			$('.cropper p.error').text('No image found').fadeIn();
		}
		if (input.files && input.files[0] && (input.files[0].type.match('image/jpg') || input.files[0].type.match('image/jpeg') || input.files[0].type.match('image/png'))) {
			var reader = new FileReader();
			reader.onload = function(e) {
				img.attr('src', e.target.result).removeAttr('style').css({
					'max-width':	'none',
					'max-height':	'none'
				}).show();
				var scaleX = $('.cropper img').prop('naturalWidth') / $('.cropper img').width(),
					scaleY = $('.cropper img').prop('naturalHeight') / $('.cropper img').height();
				if (scaleX > scaleY) {
					img.css({
						'width':	img.width() * scaleX / scaleY + 'px'
					});
				}
				fig.addClass('active');
				$('.cropper button').removeClass('disabled');
				$('.cropper p.error').text('').fadeOut();
				img.on('error', function() {
					if (input.value !== '') {
						img.attr('src', "").removeAttr('style').hide();
						fig.removeClass('active');
						$('.cropper button').addClass('disabled');
						$('.cropper p.error').text('Invalid image').fadeIn();
					}
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
	});
}

// GET VARIABLES IN URL

function getUrlVars() {
	var vars = window.location.href.split('-');
	return vars;
}

// MOVE IMAGE TO CROP

function moveImage(e) {
	if ($('.cropper img').attr('src') !== "") {
		var x	= e.pageX - $('.cropper figure').offset().left,
			y	= e.pageY - $('.cropper figure').offset().top;
		if (old_x === undefined && old_y === undefined) {
			old_x = x;
			old_y = y;
		}
		$('.cropper img').css({
			'top':		'-=' + (old_y - y) + "px",
			'left':		'-=' + (old_x - x) + "px",
			'bottom':	'auto',
			'right':	'auto'
		});
		old_y = y;
		old_x = x;
	} else {
		old_x = undefined;
		old_y = undefined;
	}
}

function moveOnMobile(e) {
	var touch = e.touches[0];
	if (touch)
		moveImage(touch);
}

// CAPITALIZE EACH WORD

function ucwords(str){
	str = str.toLowerCase();
	return str.replace(/(\b)([a-zA-Z])/g, function(firstLetter){
		return firstLetter.toUpperCase();
	});
}

// LOADER

function loader() {
	$('body').addClass('noscroll');
	$('div.background, .loader').fadeIn();
}

function loader_stop() {
	$('body').removeClass('noscroll');
	$('div.background, .loader').fadeOut();
}

// SLIDER

function switchImage() {
	timer = setInterval(function() {
		var diff = ($('.points div.selected + div').length) ? 1 : -($('.points div').length - 1);
		$(($('.points div.selected + div').length) ? $('.points div.selected + div') : $('.points div:first')).addClass('selected').siblings().removeClass('selected');
		$('.images img').animate({
			left:	'-=' + (diff * 100) + '%'
		}, 500);
	}, 5000);
}

// FORM FUNCTIONS

function containInput(container, type) {
	return (container.find('input[type=' + type + ']').length);
}

function isEmpty(elem) {
	if (containInput(elem, 'text') || containInput(elem, 'password') || elem.find('select, textarea'))
		return (elem.find('input, textarea, select').val() === '');
	else if (containInput(elem, 'radio') || containInput(elem, 'checkbox'))
		return (elem.find('input:checked').length != 1);
}

function removeInfos(elem, type) {
	elem.removeClass(type);
	elem.children().remove('.' + type);
}

function resetInput(elem) {
	removeInfos(elem, 'error');
	removeInfos(elem, 'success');
	removeInfos(elem, 'low');
	removeInfos(elem, 'middle');
	removeInfos(elem, 'high');
}

function addInteraction(elem, type, str) {
	if (!elem.hasClass(type) && str !== null)
		elem.addClass(type).append('<p class="' + type + '">' + str + '</p>');
	else if (str !== null)
		elem.children('p.' + type).html(str);
	else
		elem.addClass(type);
}

function changePassword(elem) {
	if (elem.find('input').attr('name') === 'password' || elem.find('input').attr('name') === 'confirm_password' || elem.find('input').attr('name') === 'old_password') {
		if ($('input[name=password]').val() === '' && $('input[name=confirm_password]').val() === '' && $('input[name=old_password]').val() === '')
			return true;
		else
			return false;
	} else
		return false;
}
