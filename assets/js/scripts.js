$(function(){

	// ERROR / SUCCESS INFORMATION

	$('#success, #error').click(function() {
		$(this).fadeOut();
	});

	// INPUT ICONS

	$('.input').click(function() {
		$(this).children('input').focus().val($(this).children('input').val());
	});


	// MOBILE NAV

	$('header button').click(function() {
		$(this).toggleClass('checked');
	});

	// INPUT TYPE FILE CUSTOM

	$('label[for=cover]').click(function() {
		customFileButton('cover');
	});

	// OPEN / CLOSE CROPPER OR DIALOG

	$('div.background, .cropper .icon-cross, .dialog .icon-cross, .dialog button + button').click(function (){
		if (cropper === true) {
			$('div.background, .cropper').fadeOut();
			cropper = false;
			$('input[type=file]').val("");
			$('#text-content').text("Choose an image...");
			$('.cropper img').attr('src', "").removeAttr('style');
			$('.cropper figure').removeClass('active');
			$('.cropper button').addClass('disabled');
			$('.cropper p.error').text('').fadeOut();
			$('body').removeClass('noscroll');
		} else
			$('div.background, .dialog, .colorscheme').fadeOut();
	});

	$('.profile button#delete').click(function() {
		$('.dialog.delete, div.background').fadeIn();
	});

	// CROPPER

	$('.cropper figure').mousemove(function(e) {
		e.preventDefault();
		if (e.buttons === 1)
			moveImage(e);
	});

	$('.cropper figure').mouseup(function(e) {
		e.preventDefault();
		old_x = undefined;
		old_y = undefined;
	});

	$(window).bind('touchmove', function(e) {
		$('.cropper').css({
			'overflow': 'hidden'
		});
		moveOnMobile(window.event);
	});

	$(window).bind('touchend', function(e) {
		$('.cropper').css({
			'overflow': 'auto'
		});
		old_x = undefined;
		old_y = undefined;
	});

	$('.options span').click(function(e) {
		e.preventDefault();
		var ratio = $('.cropper img').width() / $('.cropper img').height();
		if ($(this).attr('action') === 'zoom') {
			$('.cropper img').css({
				'width':		'+=50px',
				'height':		'auto',
				'left':			'-=25px'
			});
		} else if ($(this).attr('action') === 'unzoom' && $('.cropper img').width() - 50 > 215 && ($('.cropper img').width() - 50) / ratio > 305) {
			$('.cropper img').css({
				'width':	'-=50px',
				'height':	'auto',
				'left':		'+=25px'
			});
		}
	});

	// LOCATION AUTOCOMPLETE

	$('ul.location_autocomplete').on('click', 'li', function() {
		$('#search-location').val($(this).text());
		$('ul.location_autocomplete').hide();
		selected = 0;
	});

	$('body .profile *:not(.location_autocomplete)').click(function() {
		$('ul.location_autocomplete').hide();
		selected = 0;
	});

	$('#search-location').keyup(function(e) {
		var locations = [];
		$('ul.location_autocomplete li').each(function() {
			locations.push($(this).text());
		});
		if (e.which === 38 || e.which === 40) {
			var count = $('ul.location_autocomplete li').length;
			if ($('ul.location_autocomplete').is(':visible') && count > 0) {
				if (e.which === 38 && selected > 1)
					selected--;
				else if (e.which === 38 && (selected === 1 || selected === 0))
					selected = count;
				else if (e.which === 40 && selected < count)
					selected++;
				else
					selected = 1;
				$('#search-location').val($('ul.location_autocomplete li').eq(selected - 1).text());
				$('ul.location_autocomplete').scrollTop($('ul.location_autocomplete li').eq(selected - 1).position().top - $('ul.location_autocomplete li:first').position().top);
			}
		} else if (e.which === 13 && $.inArray($('#search-location').val(), locations) === 1) {
			$('ul.location_autocomplete').html("").hide();
			selected = 0;
		} else {
			$.vicopo($('#search-location').val(), function (input, cities) {
				$('ul.location_autocomplete').html("").hide();
				$.each(cities, function(index, value) {
					$('ul.location_autocomplete').show().append('<li>' + value.code + ' (' + ucwords(value.city.replace(/\d+/,'').replace('-E-ARRONDISSEMENT', '').replace('-ER-ARRONDISSEMENT', '')) + ', France)' + '</li>');
				});
			});
			selected = 0;
		}
	});

	// SLIDER

	if ($('.points div').length > 1 && $('.points div.selected').length === 1) {
		switchImage();
	}

	$('.points').on('click', 'div:not(.selected)', function(e) {
		e.preventDefault();
		clearInterval(timer);
		var diff = $(this).index() - $('.points div.selected').index();
		$(this).addClass('selected').siblings().removeClass('selected');
		$('.images img').animate({
			left:	'-=' + (diff * 100) + '%'
		}, 500);
		switchImage();
	});

	// REGISTER FORM

	$('#register input, #new_password input').keyup(function() {
		if ($(this).attr('name') === 'password') {
			$('input[name=confirm_password]').val("");
			resetInput($('input[name=confirm_password]').closest('div'));
		} else if ($(this).attr('name') === 'birth_date')
			$('#ui-datepicker-div').fadeOut();
		completedRegisterForm($(this).closest('div'));
	});

	$('#register input').change(function() {
		completedRegisterForm($(this).closest('div'));
	});

	$('#register label').click(function() {
		$(this).siblings('input').prop('checked', 'checked').parent().siblings('.radio').children('input').removeAttr('checked');
		completedRegisterForm($(this).closest('div').closest('div'));
	});

	// FORGOT FORM

	$('#forgot input').keyup(function() {
		if ($(this).val() === "") {
			addInteraction($('form > div'), 'error', "Fill this information");
			$('form input[type=submit]').prop('disabled', true);
		} else {
			resetInput($('form > div'));
			$('form input[type=submit]').prop('disabled', false);
		}
	});

	// LOGIN FORM

	$('#login input').keyup(function() {
		if ($(this).val() === "") {
			addInteraction($(this).closest('div'), 'error', "Fill this information");
			$('form input[type=submit]').prop('disabled', true);
		} else {
			resetInput($(this).closest('div'));
			var valid = true;
			$('form > div').each(function() {
				if (isEmpty($(this))) {
					valid = false;
					$('form input[type=submit]').prop('disabled', true);
					resetInput($(this).closest('div'));
				}
			});
			if (valid === true)
				$('form input[type=submit]').prop('disabled', false);
		}
	});

	// PROFILE FORM

	$('.profile form').submit(function(e) {
		e.preventDefault();
		e.stopPropagation();
	});

	$('.profile input:not([name=centers_of_interest]), .profile textarea').keyup(function() {
		if ($(this).attr('name') === 'password') {
			$('input[name=confirm_password]').val("");
			resetInput($('input[name=confirm_password]').closest('div'));
		} else if ($(this).attr('name') === 'birth_date')
			$('#ui-datepicker-div').fadeOut();
		completedProfileForm($(this).closest('div'));
	});

	$('.profile input:not([name=centers_of_interest]), .profile select').change(function() {
		completedProfileForm($(this).closest('div'));
	});

	$('.profile label').click(function() {
		$(this).siblings('input').prop('checked', 'checked').parent().siblings('.radio').children('input').removeAttr('checked');
		completedProfileForm($(this).closest('div').closest('div'));
	});

	// GEOLOCATION

	$('.icon-location').click(function() {
		$.getJSON("http://ip-api.com/json", function(data) {
			$('input[name=location]').val(data.zip + ' (' + data.city + ', ' + data.country + ')');
			completedProfileForm($('input[name=location]').closest('div'));
		});
		selected = 0;
	});

	// THEMES

	$('header .themes').click(function(e) {
		e.preventDefault();
		if (cropper === true) {
			$('.cropper').fadeOut();
			cropper = false;
			$('input[type=file]').val("");
			$('#text-content').text("Choose an image...");
			$('.cropper img').attr('src', "").removeAttr('style');
			$('.cropper figure').removeClass('active');
			$('.cropper button').addClass('disabled');
			$('.cropper p.error').text('').fadeOut();
			$('body').removeClass('noscroll');
		} else
			$('.dialog').fadeOut();
		$('.colorscheme, div.background').fadeIn();
	});

	// SEARCH FORM

	$(document).ready(function() {
		$('#age_slider div + span').html('<p>18</p>');
		$('#age_slider span + span').html('<p>115</p>');
		$('#popularity_slider div + span').html('<p>0</p>');
		$('#popularity_slider span + span').html('<p>1000</p>');
	});

	$('form#sort, form#filter').submit(function(e) {
		e.preventDefault();
	});

	// SMILEYS

	$('#send-message > span.icon-smiley-smile').click(function() {
		$('div.smileys').fadeToggle();
	});

	$('div.smileys span').click(function() {
		var smileys = ['happy', 'smile', 'tongue', 'sad', 'wink', 'grin', 'cool', 'angry', 'evil', 'shocked', 'baffled', 'confused', 'neutral', 'mustache', 'wondering', 'sleepy', 'frustrated', 'crying'],
			smiley = $(this).attr('class').split('icon-smiley-')[1];
		if ($.inArray(smiley, smileys) > -1)
			$('input[name=message]').val($('input[name=message]').val() + ':' + smiley + ':').focus();
	});

	// SOCKETS
	

});
