var io = require('socket.io').listen(8000);

io.sockets.on('connection', function(socket) {

	socket.on('new_user', function(data) {
		if (data !== null && data.user !== null)
			console.log(data.time + data.user + ' connected');
	});

	socket.on('user_interact', function(data) {
		socket.broadcast.emit(data.token, data);
	});

	socket.on('disconnection', function(data) {
		console.log(data.time + data.user + ' disconnected');
	});

});
