<?php

date_default_timezone_set('Europe/Paris');
require_once('config/db/setup.php');
require_once('models/users.php');

Class Database {

	private $_db;

	public function __construct() {
		session_name("pokedate");
		session_start();
		try {
			$this->_db = new PDO('mysql:host=localhost:8889;charset:UTF8', 'root', 'root');
			$this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$json = array (
				'database'	=> 'config/db/database.json',
				'users'		=> 'config/db/data_users.json'
			);
			if ($this->json_files_exists($json)) {
				$content = array ();
				foreach ($json as $key => $value) {
					$content[$key] = json_decode(file_get_contents($value), true);
				}
				$name = $content['database']['dbname'];
				if (isset($name) && $name != "") {
					if ($this->exist($name) === False) {
						$this->_db->exec(create_db($name, $content['database']['tables']));
						foreach ($content as $key => $value) {
							if ($key !== 'database')
								$this->_db->exec(fill_table($key, $value['rows']));
						}
					} else
						$this->_db->query("USE $name");
				} else
					echo 'Database\'s name not found in ' . $json['database'];
			} else {
				echo 'One of those files is missing : ';
				foreach ($json as $key => $value) {
					echo $value . ((isset($json[$key + 1])) ? ', ' : '.');
				}
			}
		} catch (PDOException $e) {
			echo 'File : ' . $e->getFile() . ' Line : ' . $e->getLine() . ' ' . $e->getMessage();
		}
	}

	public function __destruct() {
		$this->_db = NULL;
	}

	private function exist($database) {
		return $this->_db->query("SHOW DATABASES LIKE '$database'")->fetch() === False ? False : True;
	}

	public function getDB() {
		return $this->_db;
	}

	private function json_files_exists(array $files) {
		foreach ($files as $file) {
			if (!file_exists($file))
				return false;
		}
		return true;
	}

}

?>
