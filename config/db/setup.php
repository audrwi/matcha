<?php

function create_db($dbname, array $tables) {
	$query = "CREATE DATABASE IF NOT EXISTS $dbname;";
	$query .= "USE $dbname;";

	foreach($tables as $table) {
		$query .= "CREATE TABLE IF NOT EXISTS " . $table['name'] . " (";
		foreach ($table['params'] as $key => $param)
			$query .=  $key . " " . $param . ", ";
		$query .= "PRIMARY KEY (id));";
	}

	return $query;
}

function fill_table($table, array $rows) {
	$query = '';

	foreach ($rows as $k => $row) {
		$query .= "INSERT INTO " . $table . " (";
		foreach ($row as $key => $value) {
			$query .= $key . ($key !== 'created_at' ? ", " : ") VALUES (");
		}
		foreach ($row as $key => $value) {
			if (trim($value, "'") !== $value)
				$value = "'" . addslashes(trim($value, "'")) . "'";
			$query .= $value . ($key !== 'created_at' ? ", " : ")");
		}
		$query .= ';';
	}

	return $query;
}

?>
