<?php

require_once('config/seo/functions.php');
require_once('config/db/Database.class.php');
$db = (new Database)->getDB();

require_once('models/users.php');
require_once('models/actions.php');

$path = $_SERVER["REQUEST_URI"];
if ($path === '/favicon.ico')
	return '/assets/imgs/favicon/favicon.ico';
else if (preg_match('/\.(?:png|jpg|jpeg|gif|ico|css|js|eot|svg|ttf\?.+|woff\?.+|map)$/', $path))
	return false;
else if (preg_match('/^\/$/', $path))
	include('www/index.php');
else if (preg_match('/^\/(?:login|register|login|forgot|logout|profile|views|likes|search|chat|admin|error)$/', $path))
	include('www' . $path . '.php');
else if (preg_match('/^\/forgot-([0-9a-z]+)-([0-9a-zA-Z-_\\.]+@[0-9a-zA-Z-_\\.]+)$/', $path)) {
	list($start, $_GET['token'], $_GET['email']) = split('-', $path, 3);
	include('www/forgot.php');
} else if (preg_match('/^\/(?:activate|trainer)-([0-9a-z]+)$/', $path)) {
	$_GET[(explode('-', $path)[0] === '/trainer') ? 'username' : 'token'] = explode('-', $path)[1];
	include('www' . explode('-', $path)[0] . '.php');
} else if (preg_match('/^\/_.+\.php$/', $path) && file_exists('www/actions' . $path))
	include('www/actions' . $path);
else
	include('www/error.php');

?>
