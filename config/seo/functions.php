<?php

function getSEOInfos($page_name) {
	$json_file = "config/seo/pages.json";
	if (file_exists($json_file)) {
		$json = json_decode(file_get_contents($json_file), true);
		$pages = $json['pages'];
		foreach ($pages as $page) {
			if ($page['name'] == $page_name)
				return $page;
		}
	}
	return Null;
}

?>
