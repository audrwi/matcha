<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require_once('vendor/autoload.php');

$client = new Client(new Version1X('http://localhost:8000'));
$client->initialize();
$client->emit('new_user', [
	'user'	=> isset($_SESSION['logged_status']) && $_SESSION['logged_status'] === 0 ? ucfirst(getUserById($db, $_SESSION['logged_user_id'])['username']) : null,
	'time'	=> date('[D M d H:i:s Y] : ', time())
]);
$_SESSION['logged_status'] = 1;
$client->close();

?>
