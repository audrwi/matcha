#!/bin/bash

CURRENT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Installing Pokedate..."
ret=`python -c 'import sys; print("%i" % (sys.hexversion<0x03000000))'`
if [ $ret -eq 0 ]; then
	echo -e "\033[0;31mError\033[0m: Python is required for Pokedate"
else
	if [[ ":$PATH:" != *":$HOME/.matcha:"* ]]; then
		export PATH=$HOME/.matcha:$PATH
		echo -e "Pokedate executable path...		\033[0;32m[OK]\033[0m"
	fi
	if [ ! -d ~/.matcha ]; then
		mkdir -p ~/.matcha
	fi
	if [ ! -f ~/.matcha/pokedate ]; then
		cp $CURRENT/pokedate.py ~/.matcha/pokedate
		chmod 755 ~/.matcha/pokedate
		echo -e "Creating Pokedate command...		\033[0;32m[OK]\033[0m"
		echo
		echo "	        ▄███████████▄        "
		echo "	     ▄███▓▓▓▓▓▓▓▓▓▓▓███▄     "
		echo "	    ███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███    "
		echo "	   ██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██   "
		echo "	  ██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██  "
		echo "	 ██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██ "
		echo "	██▓▓▓▓▓▓▓▓▓███████▓▓▓▓▓▓▓▓▓██"
		echo "	██▓▓▓▓▓▓▓▓██░░░░░██▓▓▓▓▓▓▓▓██"
		echo "	██▓▓▓▓▓▓▓██░░███░░██▓▓▓▓▓▓▓██"
		echo "	███████████░░███░░███████████"
		echo "	██░░░░░░░██░░███░░██░░░░░░░██"
		echo "	██░░░░░░░░██░░░░░██░░░░░░░░██"
		echo "	██░░░░░░░░░███████░░░░░░░░░██"
		echo "	 ██░░░░░░░░░░░░░░░░░░░░░░░██ "
		echo "	  ██░░░░░░░░░░░░░░░░░░░░░██  "
		echo "	   ██░░░░░░░░░░░░░░░░░░░██   "
		echo "	    ███░░░░░░░░░░░░░░░███    "
		echo "	     ▀███░░░░░░░░░░░███▀     "
		echo "	        ▀███████████▀        "
		echo
		echo -e "\033[0;32mPokedate successfully installed !\033[0m"
	else
		if [[ ! $(diff -q $HOME/.matcha/pokedate $CURRENT/pokedate.py) ]]; then
			echo -e "\033[0;35mWarning\033[0m: Pokedate already installed on this computer"
			echo -e "Use \033[1mpokedate --help\033[0m to see how to use Pokedate command"
		else
			cp $CURRENT/pokedate.py ~/.matcha/pokedate
			chmod 755 ~/.matcha/pokedate
			echo -e "Updating Pokedate command...		\033[0;32m[OK]\033[0m"
			echo
			echo -e "\033[0;32mPokedate successfully updated !\033[0m"
		fi
	fi
fi
