<?php

function getNotSeenActionsByIdAndAction($db, $id, $action) {
	$request = $db->prepare("SELECT * FROM actions INNER JOIN users ON users.id = actions.sender
	WHERE actions.action = ? AND actions.seen = 0 AND actions.receiver = ?
	AND actions.sender NOT IN (SELECT receiver FROM actions WHERE action = 'block' AND sender = ?) AND (users.activated = 1 OR users.activated = 3)");
	$request->execute(array ($action, $id, $id));
	return $request->rowCount();
}

function getActionsByIdAndAction($db, $id, $action) {
	$request = $db->prepare("SELECT * FROM actions INNER JOIN users ON users.id = actions.sender WHERE actions.action = ? AND actions.receiver = ?
	AND actions.sender NOT IN (SELECT receiver FROM actions WHERE action = 'block' AND sender = ?) AND (users.activated = 1 OR users.activated = 3) ORDER BY actions.created_at DESC");
	$request->execute(array ($action, $id, $id));
	return $request->fetchAll();
}

function markAsSeen($db, $id, $action) {
	$request = $db->prepare("UPDATE actions SET seen = 1 WHERE receiver = ? AND action = ?");
	$request->execute(array ($id, $action));
}

function isInInteractionWithUser($db, array $ids, $action) {
	$request = $db->prepare("SELECT * FROM actions WHERE receiver = ? AND sender = ? AND action = ?");
	$request->execute(array ($ids[0], $ids[1], $action));
	return $request->rowCount() > 0;
}

function interactWithProfile($db, array $ids, $action) {
	$request = $db->prepare("INSERT INTO actions (action, sender, receiver, created_at) VALUES (?, ?, ?, NOW())");
	$request->execute(array ($action, $ids[0], $ids[1]));
}

function uninteractWithProfile($db, array $ids, $action) {
	$request = $db->prepare("DELETE FROM actions WHERE action = ? AND sender = ? AND receiver = ?");
	$request->execute(array ($action, $ids[0], $ids[1]));
}

function isAlreadyTalking($db, $id, $me) {
	$request = $db->prepare("SELECT * FROM actions WHERE action = 'message' AND
	(sender = ? OR receiver = ?) AND (sender = ? OR receiver = ?)");
	$request->execute(array ($id, $id, $me, $me));
	return $request->rowCount() > 0;
}

function getConversation($db, $id, $me) {
	$request = $db->prepare("SELECT * FROM actions WHERE action = 'message' AND
	(sender = ? OR receiver = ?) AND (sender = ? OR receiver = ?) ORDER BY created_at");
	$request->execute(array ($id, $id, $me, $me));
	return $request->fetchAll();
}

function readMessage($db, $id) {
	$request = $db->prepare("UPDATE actions SET seen = 1 WHERE id = ?");
	$request->execute(array ($id));
}

function readAllMessagesFromId($db, $id, $me) {
	$request = $db->prepare("UPDATE actions SET seen = 1 WHERE sender = ? AND receiver = ?");
	$request->execute(array ($id, $me));
}

function newMessage($db, $id, $me, $message) {
	$request = $db->prepare("INSERT INTO actions (action, sender, receiver, created_at, message) VALUES ('message', ?, ?, NOW(), ?)");
	$request->execute(array ($me, $id, $message));
	$id_action = $db->lastInsertId();
	$get = $db->prepare("SELECT * FROM actions WHERE id = ?");
	$get->execute(array ($id_action));
	return $get->fetch();
}

function getDatePrevMessage($db, $id) {
	$request = $db->prepare("SELECT * FROM actions WHERE id < ? AND (sender IN (SELECT sender FROM actions WHERE id = ?) OR sender IN (SELECT receiver FROM actions WHERE id = ?))
	AND (receiver IN (SELECT sender FROM actions WHERE id = ?) OR receiver IN (SELECT receiver FROM actions WHERE id = ?)) ORDER BY id DESC LIMIT 1");
	$request->execute(array ($id, $id, $id, $id, $id));
	return $request->rowCount() === 0 ? null : date('Y-m-d', strtotime($request->fetch()['created_at']));
}

function removeActionsById($db, $id) {
	$request = $db->prepare("DELETE FROM actions WHERE sender = ? OR receiver = ?");
	$request->execute(array ($id, $id));
}

function isReported($db, $id) {
	$request = $db->prepare("SELECT * FROM actions WHERE receiver = ? AND action = 'report'");
	$request->execute(array ($id));
	return $request->rowCount() > 0;
}

?>
