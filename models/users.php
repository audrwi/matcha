<?php

function hashPassword($str) {
	return hash('sha512', hash('whirlpool', $str) . "youwillneverfindthispasswordbitch");
}

function generateToken() {
	return time() . md5(uniqid(mt_rand(), true));
}

function idExists($db, $id) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ?");
	$request->execute(array ($id));
	return $request->rowCount() > 0;
}

function usernameExists($db, $username) {
	$request = $db->prepare("SELECT * FROM users WHERE LOWER(username) = LOWER(?)");
	$request->execute(array ($username));
	return $request->rowCount() > 0;
}

function emailExists($db, $email) {
	$request = $db->prepare("SELECT * FROM users WHERE LOWER(email) = LOWER(?)");
	$request->execute(array ($email));
	return $request->rowCount() > 0;
}

function emailExistsButNotMine($db, array $infos) {
	$request = $db->prepare("SELECT * FROM users WHERE LOWER(email) = LOWER(?) AND id != ?");
	$request->execute(array ($infos['email'], $infos['id']));
	return $request->rowCount() > 0;
}

function passwordErrors($password) {
	$errors = array ();
	if (strlen($password) < 8)
		$errors[] = "At least 8 characters";
	if (!preg_match("#[0-9]+#", $password))
		$errors[] = "At least one number";
	if (!preg_match("#[A-Z]+#", $password) || !preg_match("#[a-z]+#", $password))
		$errors[] = "At least a capital letter and a lowercase letter";
	return $errors;
}

function checkSecurityLevel(array $errors = null) {
	return count($errors) === 3 || count($errors) === 2 ? 'low' : (count($errors) === 1 ? 'middle' : 'high');
}

function isGoodConfirmation($password, $confirmation) {
	return ($password === $confirmation);
}

function checkAge($birth_date) {
	$error = "";
	$birth = new DateTime($birth_date);
	$min = new DateTime('now - 18 years');
	if (time() < strtotime($birth_date))
		$error = "You can't be born in the future";
	else if (!date_diff($min, $birth)->invert)
		$error = "Too young, you have to be adult";
	else if (date("Y-m-d") - $birth_date > 115)
		$error = "Are you a fucking vampire ?";
	return $error;
}

function ucname($string) {
	$string = ucwords(strtolower($string));
	foreach (array('-', '\'') as $delimiter) {
		if (strpos($string, $delimiter) !== false)
			$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
	}
	return $string;
}

function createUser($db, array $infos) {
	$tab = array (
		ucname(trim($infos['first_name'])),
		ucname(trim($infos['last_name'])),
		trim(strtolower($infos['username'])),
		strtolower(trim($infos['email'])),
		hashPassword($infos['password']),
		$infos['birth_date'],
		$infos['gender'],
		generateToken()
	);
	$request = $db->prepare("INSERT INTO users (first_name, last_name, username, email, password, birth_date, gender, created_at, token) VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), ?)");
	$request->execute($tab);
	return $db->lastInsertId();
}

function getUserById($db, $id) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ?");
	$request->execute(array ($id));
	return $request->fetch();
}

function getUserByToken($db, $token) {
	$request = $db->prepare("SELECT * FROM users WHERE token = ?");
	$request->execute(array ($token));
	return $request->fetch();
}

function getUserByEmail($db, $email) {
	$request = $db->prepare("SELECT * FROM users WHERE email = ?");
	$request->execute(array ($email));
	return $request->fetch();
}

function getUserByUsername($db, $username) {
	$request = $db->prepare("SELECT * FROM users WHERE username = ?");
	$request->execute(array ($username));
	return $request->fetch();
}

function updateActive($db, array $infos) {
	$request = $db->prepare("UPDATE users SET activated = ? WHERE id = ?");
	$request->execute(array ($infos['active'], $infos['id']));
}

function isValidEmailAndToken($db, array $infos) {
	$request = $db->prepare("SELECT * FROM users WHERE email = ? AND token = ?");
	$request->execute(array ($infos['email'], $infos['token']));
	return $request->rowCount() === 1;
}

function updateToken($db, $id) {
	$request = $db->prepare("UPDATE users SET token = ? WHERE id = ?");
	$request->execute(array (generateToken(), $id));
}

function updatePassword($db, array $infos) {
	$request = $db->prepare("UPDATE users SET password = ? WHERE id = ?");
	$request->execute(array (hashPassword($infos['password']), $infos['id']));
}

function updateConnection($db, array $infos) {
	$request = $db->prepare("UPDATE users SET connected = ?, last_activity = NOW() WHERE id = ?");
	$request->execute(array ($infos['connected'], $infos['id']));
}

function checkUser($db, $infos) {
	$request = $db->prepare("SELECT * FROM users WHERE (username = ? OR email = ?) AND password = ?");
	$request->execute(array ($infos['user'], $infos['user'], hashPassword($infos['password'])));
	return $request->rowCount() === 1 ? $request->fetch() : null;
}

function isUncompleted($db, $id) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ? AND (interested_by IS NULL OR interested_by = '' OR pokemon_type IS NULL OR pokemon_type = '' OR location IS NULL OR location = ''
		OR description IS NULL OR description = '' OR centers_of_interest IS NULL OR centers_of_interest = '' OR covers IS NULL OR covers = '' OR activated = 0 OR activated = 2)");
	$request->execute(array ($id));
	return $request->rowCount() === 1;
}

function getAllPokemonTypes($db) {
	$request = $db->prepare("SHOW COLUMNS FROM users WHERE Field = 'pokemon_type'");
	$request->execute();
	preg_match("/^enum\(\'(.*)\'\)$/", $request->fetch()['Type'], $result);
	return explode("','", $result[1]);
}

function getAllCentersOfInterest($db) {
	$request = $db->prepare("SELECT centers_of_interest FROM users WHERE centers_of_interest IS NOT NULL");
	$request->execute();
	return $request->fetchAll();
}

function base64ToJpeg($base64_string, $filename, array $infos) {
	ini_set('allow_url_fopen', true);
	$output_file = 'assets/imgs/users/' . $filename;
	$ifp = fopen($output_file, "wb");
	$data = explode(',', $base64_string);
	fwrite($ifp, base64_decode($data[1]));
	fclose($ifp);
	list($width, $height) = getimagesize($output_file);
	$src = $infos['type'] === 'image/jpeg' ? imagecreatefromjpeg($output_file) : imagecreatefrompng($output_file);
	$dst = imagecreatetruecolor(420, 600);
	$white = imagecolorallocate($dst, 255, 255, 255);
	imagefill($dst, 0, 0, $white);
	imagecopyresampled($dst, $src, 0, 0, $infos['x'], $infos['y'], $infos['width'] * 2, $infos['height'] * 2, $width, $height);
	imagejpeg($dst, $output_file, 100);
}

function getCovers($db, $id) {
	$request = $db->prepare("SELECT covers FROM users WHERE id = ?");
	$request->execute(array ($id));
	return $request->fetch()[0];
}

function addImage($db, $infos) {
	ini_set('memory_limit', '-1');
	base64ToJpeg($infos['image'], $infos['name'], $infos['infos']);
	$request = $db->prepare("UPDATE users SET covers = ? WHERE id = ?");
	$request->execute(array ($infos['covers'], $infos['id']));
	return $infos['name'];
}

function updateCover($db, array $infos) {
	$request = $db->prepare("UPDATE users SET covers = ? WHERE id = ?");
	$request->execute(array ($infos['covers'], $infos['id']));
}

function getTags($db, $id) {
	$request = $db->prepare("SELECT centers_of_interest FROM users WHERE id = ?");
	$request->execute(array ($id));
	return $request->fetch()[0];
}

function updateTag($db, array $infos) {
	$request = $db->prepare("UPDATE users SET centers_of_interest = ? WHERE id = ?");
	$request->execute(array ($infos['tags'], $infos['id']));
}

function updateUser($db, $id, array $infos) {
	$query = "UPDATE users SET";
	$array = array ();
	foreach ($infos as $key => $value) {
		$query .= " " . $key . " = ?,";
		$array[] = $value;
	}
	$query = substr($query, 0, -1) . " WHERE id = ?";
	$array[] = $id;
	$request = $db->prepare($query);
	$request->execute($array);
}

function getDifferenceFromNow($datetime) {
	$now = new DateTime();
	$date = new DateTime($datetime);
	$diff = $date->diff($now);
	if ($diff->y > 0)
		return $diff->y . (($diff->y > 1) ? ' years' : ' year') . ' ago';
	else if ($diff->m > 0)
		return  $diff->m . (($diff->m > 1) ? ' months' : ' month') . ' ago';
	else if ($diff->d > 0)
		return  $diff->d . (($diff->d > 1) ? ' days' : ' day') . ' ago';
	else if ($diff->h > 0)
		return  $diff->h . (($diff->h > 1) ? ' hours' : ' hour') . ' ago';
	else if ($diff->i > 0)
		return  $diff->i . (($diff->i > 1) ? ' minutes' : ' minute') . ' ago';
	else
		return 'a few seconds ago';
}

function getAge($birth_date) {
	$today = new DateTime();
	$birth = new DateTime($birth_date);
	return $birth->diff($today)->format('%y');
}

function increasePopularity($db, array $infos) {
	$request = $db->prepare("UPDATE users SET popularity = LEAST(popularity + ?, 1000) WHERE id = ?");
	$request->execute(array ($infos['points'], $infos['id']));
}

function decreasePopularity($db, array $infos) {
	$request = $db->prepare("UPDATE users SET popularity = GREATEST(0, popularity - ?) WHERE id = ?");
	$request->execute(array ($infos['points'], $infos['id']));
}

function getFirstCoverById($db, $id) {
	$request = $db->prepare("SELECT covers FROM users WHERE id = ?");
	$request->execute(array ($id));
	$covers = $request->fetch()[0];
	foreach (explode(';', $covers) as $cover) {
		if (isset(explode('?', $cover)[1]) && explode('?', $cover)[1] === 'first')
			return explode('?', $cover)[0];
	}
	return null;
}

function getAllThemes($db) {
	$request = $db->prepare("SHOW COLUMNS FROM users WHERE Field = 'colorscheme'");
	$request->execute();
	preg_match("/^enum\(\'(.*)\'\)$/", $request->fetch()['Type'], $result);
	return explode("','", $result[1]);
}

function updateTheme($db, array $infos) {
	$request = $db->prepare("UPDATE users SET colorscheme = ? WHERE id = ?");
	$request->execute(array ($infos['theme'], $infos['id']));
}

function getClosestsUsers($db, $id) {
	$request = $db->prepare("SELECT *, SQRT(POW(69.1 * (latitude - (SELECT latitude FROM users WHERE id = ?)), 2) +
	POW(69.1 * ((SELECT longitude FROM users WHERE id = ?) - longitude) * COS(latitude / 57.3), 2)) AS distance
	FROM users WHERE id != ? AND (activated = 1 OR activated = 3) AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	(interested_by IN (SELECT gender FROM users WHERE id = ?) OR interested_by = 'both') AND
	(gender IN (SELECT interested_by FROM users WHERE id = ?) OR (SELECT interested_by FROM users WHERE id = ?) = 'both')
	ORDER BY distance LIMIT 10");
	$request->execute(array ($id, $id, $id, $id, $id, $id, $id, $id));
	return $request->fetchAll();
}

function getAllValidUsers($db, $id, $more) {
	$request = $db->prepare("SELECT * FROM users WHERE id != ? AND (activated = 1 OR activated = 3) AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	(interested_by IN (SELECT gender FROM users WHERE id = ?) OR interested_by = 'both') AND
	(gender IN (SELECT interested_by FROM users WHERE id = ?) OR (SELECT interested_by FROM users WHERE id = ?) = 'both')" . $more .
	"ORDER BY RAND()");
	$request->execute(array ($id, $id, $id, $id, $id, $id));
	return $request->fetchAll();
}

function getCustomUsers($db, $array, $req) {
	$request = $db->prepare($req);
	$request->execute($array);
	return $request->fetchAll();
}

function isValidUser($db, $id, $me) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ? AND (activated = 1 OR activated = 3) AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	(interested_by IN (SELECT gender FROM users WHERE id = ?) OR interested_by = 'both') AND
	(gender IN (SELECT interested_by FROM users WHERE id = ?) OR (SELECT interested_by FROM users WHERE id = ?) = 'both')");
	$request->execute(array ($id, $me, $me, $me, $me, $me));
	return $request->rowCount() === 1;
}

function isTalkingUser($db, $id, $me) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ? AND (activated = 1 OR activated = 3) AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	id IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'like') AND
	id IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'like') ORDER BY username ASC");
	$request->execute(array ($id, $me, $me, $me, $me));
	return $request->rowCount() === 1;
}

function getUsersToTalk($db, $id) {
	$request = $db->prepare("SELECT users.*,
	(SELECT COUNT(*) FROM actions WHERE actions.sender = users.id AND receiver = ? AND action = 'message' AND seen = 0) AS count
	FROM users WHERE (activated = 1 OR activated = 3) AND id != ? AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	id IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'like') AND
	id IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'like') ORDER BY username ASC");
	$request->execute(array ($id, $id, $id, $id, $id, $id));
	return $request->fetchAll();
}

function getAllReportedusers($db) {
	$request = $db->prepare("SELECT users.*,
	(SELECT COUNT(*) FROM actions WHERE actions.receiver = users.id AND actions.action = 'report') AS count FROM users
	WHERE (activated = 1 OR activated = 3) AND id IN (SELECT receiver FROM actions WHERE action = 'report')
	ORDER BY count DESC");
	$request->execute();
	return $request->fetchAll();
}

function getReportersById($db, $id) {
	$request = $db->prepare("SELECT * FROM users WHERE (activated = 1 OR activated = 3) AND
	id IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'report')");
	$request->execute(array ($id));
	return $request->fetchAll();
}

function isConnected($db, $id) {
	$request = $db->prepare("SELECT * FROM users WHERE id = ? AND connected = 1");
	$request->execute(array ($id));
	return $request->rowCount() === 1;
}

function newSocket($db, $id) {
	$socket = generateToken();
	$request = $db->prepare("UPDATE users SET socket = ? WHERE id = ?");
	$request->execute(array ($socket, $id));
	return $socket;
}

function deleteSocket($db, $id) {
	$request = $db->prepare("UPDATE users SET socket = NULL WHERE id = ?");
	$request->execute(array ($id));
}

?>
