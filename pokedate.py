#!/usr/bin/python

import subprocess
import signal
import sys
import time
import os
import os.path
from multiprocessing import Process

error = ""
def signal_handler(signum, frame):
	try:
		print
		os.system('/usr/local/mysql/bin/mysql -uroot -proot -D pokedate -e "update users set connected = 0 and last_activity = now() where connected = 1;" &> /dev/null');
		os.system('/usr/local/mysql/bin/mysql -uroot -proot -D pokedate -e "update users set socket = null where socket is not null;" &> /dev/null');
		print('[' + time.strftime("%a %b %d %H:%M:%S %Y") + '] : All users disconnected')
		print('[' + time.strftime("%a %b %d %H:%M:%S %Y") + '] : Socket server OFF')
	except:
		sys.exit(1)
def start_server():
	p = subprocess.Popen(["php -v"], stdout=subprocess.PIPE, shell=True)
	(output, error) = p.communicate()
	os.system('/usr/local/mysql/bin/mysql -uroot -proot -D pokedate -e "update users set connected = 0 and last_activity = now() where connected = 1;" &> /dev/null');
	os.system('/usr/local/mysql/bin/mysql -uroot -proot -D pokedate -e "update users set socket = null where socket is not null;" &> /dev/null');
	print('PHP ' + output[4:10] + ' Development Server started at ' + time.strftime("%a %b %d %H:%M:%S %Y"))
	print('Listening on http://localhost:8888')
	print('Document root is ' + os.getcwd())
	print('[' + time.strftime("%a %b %d %H:%M:%S %Y") + '] : Socket server ON')
	print('Press Ctrl-C to quit')
	os.system("php -S localhost:8888 " + os.getcwd() + "/config/router.php  &> /dev/null & node " + os.getcwd() + "/assets/js/server.js")

if len(sys.argv[1:]):
	if len(sys.argv[2:]) == 0:
		if sys.argv[1].strip() == "server:start":
			if os.path.exists(os.getcwd() + "/pokedate.py") == False:
				error = "You don't execute the pokedate command from the root of the project folder"
			else:
				if os.popen('curl -s localhost:8080').read() == "":
					error = "Please start PAMP"
				else:
					signal.signal(signal.SIGINT, signal_handler)
					Process(target=start_server).start()
		elif sys.argv[1].strip() == "db:clear":
			os.system('/usr/local/mysql/bin/mysql -uroot -proot -e "drop database pokedate;" &> /dev/null');
			print("Pokedate database successfully deleted")
		elif sys.argv[1].strip() == "--help":
			print(">> Welcome to Pokedate Helper <<".center(72, ' '))
			print("/!\ Start PAMP before starting the server /!\\".center(72, ' '))
			print("/!\ Execute the command from the root of the project folder /!\\".center(72, ' '))
			print
			print("\033[3mpokedate server:start\033[0m\t\tStart the server (http://localhost:8888)")
			print("\033[3mpokedate db:clear\033[0m\t\tClear the database")
			print("\033[3mpokedate --help\033[0m\t\t\tView the helper")
			print
			print("Created by aboucher @ 42".center(72, ' '))
		else:
			error = "Invalid argument"
	else:
		error = "Only one argument required"
else:
	error = "One argument required"

if len(error):
	print("\033[0;31mError\033[0m: " + error)
	print("Use \033[1mpokedate --help\033[0m to see how to use Pokedate command")
