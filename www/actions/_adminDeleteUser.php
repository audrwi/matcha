<?php

if (isset($_POST['id']) && $_POST['id'] !== "" && !empty(getUserById($db, $_POST['id'])) && isReported($db, $_POST['id'])) {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			if (getUserById($db, $_SESSION['logged_user_id'])['admin'] == 1) {

				updateActive($db, array ('id' => $_POST['id'], 'active' => 2));
				removeActionsById($db, $_POST['id']);

			} else
				echo 'You\'re not allowed to delete an account.';

		} else
			echo 'Time out.';
	} else
		echo 'Something went wrong.';

} else
	echo 'Invalid user.';

?>
