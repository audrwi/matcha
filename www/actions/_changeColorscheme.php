<?php

if (isset($_POST['theme']) && $_POST['theme'] !== '') {

	$themes = getAllThemes($db);
	if (in_array($_POST['theme'], $themes))
		updateTheme($db, array ('id' => $_SESSION['logged_user_id'], 'theme' => $_POST['theme']));
	else
		echo 'Invalid theme';

} else
	echo 'Missing informations';

?>
