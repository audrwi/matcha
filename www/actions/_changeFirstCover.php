<?php

if (isset($_POST['image']) && $_POST['image'] !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$covers = getCovers($db, $_SESSION['logged_user_id']);
			$explodeCovers = explode(';', $covers);
			$covers = '';
			foreach ($explodeCovers as $cover) {
				if (explode("?", $cover)[0] === $_POST['image'])
					$covers .= explode("?", $cover)[0] . '?first;';
				else if ($cover !== '')
					$covers .= explode("?", $cover)[0] . ';';
			}
			updateCover($db, array ('id' => $_SESSION['logged_user_id'], 'covers' => $covers));

		} else
			echo 'Time out';
	} else
		echo 'Something went wrong';

}

?>
