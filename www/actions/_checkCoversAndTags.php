<?php

$tags = getTags($db, $_SESSION['logged_user_id']);
$nbTags = count(explode(';', $tags));
$covers = getCovers($db, $_SESSION['logged_user_id']);
$nbCovers = count(explode(';', $covers));

if ($tags === null || $tags === '' || $nbTags === 0 || $covers === '' || $covers === null || $nbCovers === 0)
	echo 'false';
else
	echo 'true';

?>
