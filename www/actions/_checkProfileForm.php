<?php

if (isset($_POST['name']) && $_POST['name'] != "" && isset($_POST['value']) && $_POST['value']) {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$regex = array (
				"first_name"			=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
				"last_name"				=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
				"email"					=> '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i',
				"birth_date"			=> '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',
				"location"				=> '/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}/',
				"description"			=> '/^(?s:.){1,120}$/'
			);
			$user = getUserById($db, $_SESSION['logged_user_id']);

			if (array_key_exists($_POST['name'], $regex) && !preg_match($regex[$_POST['name']], trim($_POST['value'])))
				echo 'error';
			else if ($_POST['name'] === 'email' && emailExistsButNotMine($db, array ('id' => $_SESSION['logged_user_id'], 'email' => $_POST['value'])))
				echo 'This email already exists';
			else if ($_POST['name'] === 'birth_date' && checkAge($_POST['value']) != "")
				echo checkAge($_POST['value']);
			else if ($_POST['name'] === 'password')
				echo checkSecurityLevel(passwordErrors($_POST['value']));
			else if ($_POST['name'] === 'old_password') {
				if (hashPassword($_POST['value']) === $user['password'])
					echo 'same';
			} else if ($_POST['name'] === 'sexual_orientation') {
				if ($_POST['value'] === $user['interested_by'])
					echo 'same';
			} else if ($user[$_POST['name']] === $_POST['value'])
					echo 'same';

		} else
			echo 'error_token';

	} else
		echo 'error_token';

}

?>
