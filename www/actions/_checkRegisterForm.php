<?php

if (isset($_POST['name']) && $_POST['name'] != "" && isset($_POST['value']) && $_POST['value']) {

	$regex = array (
		"first_name"	=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
		"last_name"		=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
		"email"			=> '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i',
		"username"		=> '/^[a-z0-9]{5,15}$/i',
		"birth_date"	=> '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/'
	);

	if (array_key_exists($_POST['name'], $regex) && !preg_match($regex[$_POST['name']], $_POST['value']))
		echo 'error';
	else if ($_POST['name'] === 'email' && emailExists($db, $_POST['value']))
		echo 'This email already exists';
	else if ($_POST['name'] === 'username' && usernameExists($db, $_POST['value']))
		echo 'This username already exists';
	else if ($_POST['name'] === 'birth_date' && checkAge($_POST['value']) != "")
		echo checkAge($_POST['value']);
	else if ($_POST['name'] === 'password')
		echo checkSecurityLevel(passwordErrors($_POST['value']));

}

?>
