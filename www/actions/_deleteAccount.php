<?php

if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

	$old_timestamp = time() - (15 * 60);
	if ($_SESSION['token_time'] >= $old_timestamp) {

		updateActive($db, array ('id' => $_SESSION['logged_user_id'], 'active' => 2));
		removeActionsById($db, $_SESSION['logged_user_id']);

	} else
		echo 'Time out';
} else
	echo 'Something went wrong';

?>
