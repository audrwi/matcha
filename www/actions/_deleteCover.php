<?php

if (isset($_POST['image']) && $_POST['image'] !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$covers = getCovers($db, $_SESSION['logged_user_id']);
			$explodeCovers = explode(';', $covers);
			$first = false;
			$num = -1;
			foreach ($explodeCovers as $key => $cover) {
				if (explode("?", $cover)[0] === $_POST['image'] && explode("?", $cover)[1] === "first") {
					$first = true;
					$num = $key;
				}
			}
			$covers = '';
			foreach ($explodeCovers as $key => $cover) {
				if ($first === true) {
					if (explode("?", $cover)[0] !== $_POST['image'] && explode("?", $cover)[0] != '') {
						$covers .= explode("?", $cover)[0];
						$covers .= ($key === 0 || ($num === 0 && $key === 1))  ? '?first;' : ';';
					}
				} else if ($cover !== $_POST['image'] && $cover != '')
					$covers .= $cover . ';';
			}
			updateCover($db, array ('id' => $_SESSION['logged_user_id'], 'covers' => (count($explodeCovers) > 1) ? $covers : null));
			unlink('/assets/imgs/users/' . $_POST['image']);

		} else
			echo 'Time out';
	} else
		echo 'Something went wrong';

}

?>
