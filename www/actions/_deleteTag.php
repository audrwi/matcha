<?php

if (isset($_POST['tag']) && $_POST['tag'] !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$tags = getTags($db, $_SESSION['logged_user_id']);
			$explodeTags = explode(';', $tags);
			$already = false;
			$new = '';
			foreach ($explodeTags as $tag) {
				if ($tag === $_POST['tag'])
					$already = true;
				else if ($tag !== '')
					$new .= $tag . ';';
			}
			if ($already === true) {
				updateTag($db, array ('id' => $_SESSION['logged_user_id'], 'tags' => (count($explodeTags) > 1) ? $new : null));
			} else
				echo 'An error occurred.';

		} else
			echo 'An error occurred.';
	} else
		echo 'An error occurred.';

} else
	echo 'An error occurred.';

?>
