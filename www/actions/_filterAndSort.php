<?php

$users = null;

if (isset($_POST['sortBy']) && isset($_POST['filters']['place']) && isset($_POST['filters']['tags']) && isset($_POST['filters']['type']) && isset($_POST['filters']['age']) && isset($_POST['filters']['pop']) &&
	isset($_POST['filters']['placeVal']) && isset($_POST['filters']['tagsVal']) && isset($_POST['filters']['typeVal']) && isset($_POST['filters']['ageVal']) && isset($_POST['filters']['popVal']) &&
	($_POST['sortBy'] === 'place' || $_POST['sortBy'] === 'tags' || $_POST['sortBy'] === 'age' || $_POST['sortBy'] === 'popularity') &&
	!($_POST['sortBy'] === 'place' && $_POST['filters']['place'] === 'false' && $_POST['filters']['tags'] === 'false' && $_POST['filters']['type'] === 'false' && $_POST['filters']['age'] === false && $_POST['filters']['pop'] === 'false')) {

	$id = $_SESSION['logged_user_id'];
	$restrictions = "WHERE id != ? AND (activated = 1 OR activated = 3) AND
	id NOT IN (SELECT sender FROM actions WHERE receiver = ? AND action = 'block') AND
	id NOT IN (SELECT receiver FROM actions WHERE sender = ? AND action = 'block') AND
	(interested_by IN (SELECT gender FROM users WHERE id = ?) OR interested_by = 'both') AND
	(gender IN (SELECT interested_by FROM users WHERE id = ?) OR (SELECT interested_by FROM users WHERE id = ?) = 'both') ";
	$limit = "LIMIT 10";
	$filters = "";
	$array = array ($id, $id, $id, $id, $id, $id);

	$regex = array (
		"place"		=> '/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}/',
		"tags"		=> '/^[;]{0,}([a-z0-9]{1,15}[;]+){1,3}$/i',
		"age"		=> '/^[0-9]{1,3} - [0-9]{1,3}$/i',
		"pop"		=> '/^[0-9]{1,4} - [0-9]{1,4}$/i'
	);
	$types = getAllPokemonTypes($db);

	if ($_POST['filters']['place'] === 'true') {
		if (!preg_match($regex['place'], $_POST['filters']['placeVal']))
			exit();
		$code = substr($_POST['filters']['placeVal'], 0, 5);
		$filters .= "AND location = '" . $code . "' ";
	}
	if ($_POST['filters']['tags'] === 'true') {
		if (!preg_match($regex['tags'], $_POST['filters']['tagsVal']))
			exit();
		foreach (explode(';', $_POST['filters']['tagsVal']) as $tag) {
			if ($tag !== '')
				$filters .= "AND (centers_of_interest LIKE '%;" . $tag . ";%' OR centers_of_interest LIKE '" . $tag . ";%') ";
		}
	}
	if ($_POST['filters']['type'] === 'true') {
		if (!in_array(ucwords($_POST['filters']['typeVal']), $types))
			exit();
		$filters .= "AND pokemon_type = '" . ucwords($_POST['filters']['typeVal']) . "' ";
	}
	if ($_POST['filters']['age'] === 'true') {
		if (!preg_match($regex['age'], $_POST['filters']['ageVal']))
			exit();
		list($min, $max) = explode(" - ", $_POST['filters']['ageVal']);
		if ($min < 18 || $max > 115)
			exit();
		$filters .= "AND DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), birth_date)), '%Y') >= " . $min . " AND DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), birth_date)), '%Y') <= " . $max . " ";
	}
	if ($_POST['filters']['pop'] === 'true') {
		if (!preg_match($regex['pop'], $_POST['filters']['popVal']))
			exit();
		list($min, $max) = explode(" - ", $_POST['filters']['popVal']);
		if ($min < 0 || $max > 1000)
			exit();
		$filters .= "AND popularity >= " . $min . " AND popularity <= " . $max . " ";
	}

	if ($_POST['sortBy'] === 'place') {
		$start = "SELECT *, SQRT(POW(69.1 * (latitude - (SELECT latitude FROM users WHERE id = ?)), 2) +
		POW(69.1 * ((SELECT longitude FROM users WHERE id = ?) - longitude) * COS(latitude / 57.3), 2)) AS distance
		FROM users ";
		$order = "ORDER BY distance ";
		$array[] = $id;
		$array[] = $id;
	} else if ($_POST['sortBy'] === 'tags') {
		$tmp = getAllValidUsers($db, $id, $filters);
		$tags = getTags($db, $id);
		$explodeTags = explode(';', $tags);
		$results = array ();
		foreach ($explodeTags as $tag) {
			foreach ($tmp as $user) {
				foreach (explode(';', $user['centers_of_interest']) as $tag2) {
					if ($tag === $tag2)
						$results[$user['id']] = (array_key_exists($user['id'], $results)) ? $results[$user['id']] + 1 : 1;
				}
			}
		}
		if (count($results) > 0) {
			arsort($results);
			$users = array ();
			$i = 0;
			foreach ($results as $key => $value) {
				if ($i < 10) {
					$user = getUserById($db, $key);
					$users[] = array (
						'id'		=> $user['id'],
						'cover'		=> getFirstCoverById($db, $user['id']),
						'username'	=> $user['username']
					);
				}
				$i++;
			}
		}
	} else if ($_POST['sortBy'] === 'age') {
		$start = "SELECT *, DATEDIFF((SELECT birth_date FROM users WHERE id = ?), birth_date) AS age_difference FROM users ";
		$order = "ORDER BY age_difference ";
		$array[] = $id;
	} else if ($_POST['sortBy'] === 'popularity') {
		$start = "SELECT * FROM users ";
		$order = "ORDER BY popularity DESC ";
	}

	if ($_POST['sortBy'] !== 'tags') {
		$search = getCustomUsers($db, $array, $start . $restrictions . $filters . $order . $limit);
		if (count($search) > 0) {
			$users = array ();
			foreach ($search as $user) {
				$users[] = array (
					'id'		=> $user['id'],
					'cover'		=> getFirstCoverById($db, $user['id']),
					'username'	=> $user['username']
				);
			}
		}
	}

} else {

	$default = getClosestsUsers($db, $id);
	if (count($default) > 0) {
		$users = array ();
		foreach ($default as $user) {
			$users[] = array (
				'id'		=> $user['id'],
				'cover'		=> getFirstCoverById($db, $user['id']),
				'username'	=> $user['username']
			);
		}
	}

}

echo json_encode($users);

?>
