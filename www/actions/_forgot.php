<?php

require_once('www/emailings/forgot.php');

if (isset($_POST['email']) && $_POST['email'] !== "") {

	$regex = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
	if (preg_match($regex, $_POST['email']) && emailExists($db, $_POST['email'])) {

		$user = getUserByEmail($db, $_POST['email']);
		updateToken($db, $user['id']);
		updateActive($db, array ('id' => $user['id'], 'active' => 3));
		$account = getUserById($db, $user['id']);
		$start = 'http://' . str_replace("_forgot.php", "", $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		$header = 'From: "PokeDate" <aboucher@student.42.fr>' . "\r\n";
		$header .= 'Reply-To: <aboucher@student.42.fr>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$content = getForgotMail($start, $account['username'], $account['token'], $_POST['email']);
		mail($_POST['email'], "PokeDate - Reset your password", $content, $header);

	} else
		echo 'Invalid email address';

} else
	echo "Fill this information";

?>
