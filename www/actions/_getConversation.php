<?php

if (isset($_POST['id']) && $_POST['id']) {

	if ($_POST['id'] !== $_SESSION['logged_user_id'] || isTalkingUser($db, $_POST['id'], $_SESSION['logged_user_id'])) {

		$smileys = array (':happy:', ':smile:', ':tongue:', ':sad:', ':wink:', ':grin:', ':cool:', ':angry:', ':evil:', ':shocked:', ':baffled:', ':confused:', ':neutral:', ':mustache:', ':wondering:', ':sleepy:', ':frustrated:', ':crying:');
		$replace = array ('<span class="icon-smiley-happy"></span>', '<span class="icon-smiley-smile"></span>', '<span class="icon-smiley-tongue"></span>', '<span class="icon-smiley-sad"></span>', '<span class="icon-smiley-wink"></span>', '<span class="icon-smiley-grin"></span>', '<span class="icon-smiley-cool"></span>', '<span class="icon-smiley-angry"></span>', '<span class="icon-smiley-evil"></span>', '<span class="icon-smiley-shocked"></span>', '<span class="icon-smiley-baffled"></span>', '<span class="icon-smiley-confused"></span>', '<span class="icon-smiley-neutral"></span>', '<span class="icon-smiley-mustache"></span>', '<span class="icon-smiley-wondering"></span>', '<span class="icon-smiley-sleepy"></span>', '<span class="icon-smiley-frustrated"></span>', '<span class="icon-smiley-crying"></span>');
		$messages = array ();
		if (isAlreadyTalking($db, $_POST['id'], $_SESSION['logged_user_id'])) {
			foreach (getConversation($db, $_POST['id'], $_SESSION['logged_user_id']) as $conv) {
				if ($conv['sender'] !== $_SESSION['logged_user_id'])
					readMessage($db, $conv['id']);
				$date = date('Y-m-d', strtotime($conv['created_at']));
				$messages[] = array (
					'sender'		=> $conv['sender'] === $_SESSION['logged_user_id'] ? 'me' : 'other',
					'message'		=> str_replace($smileys, $replace, $conv['message']),
					'date'			=> date('Y-m-d', time()) === $date ? 'today' : (date('Y-m-d', time() - 60 * 60 * 24) === $date ? 'yesterday' : $date),
					'hour'			=> date('H:i', strtotime($conv['created_at']))
				);
			}
			echo json_encode($messages);
		} else
			echo json_encode(null);

	}

}

?>
