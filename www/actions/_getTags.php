<?php

$array = array ();
$centers = getAllCentersOfInterest($db);
$all = "";

if ($centers !== "") {
	foreach ($centers as $center) {
		$all .= $center[0];
	}
	foreach (explode(';', $all) as $center) {
		$array[] = $center;
	}
	$array = array_filter(array_unique($array));
	sort($array);
}

echo json_encode($array);

?>
