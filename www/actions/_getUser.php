<?php

if (isset($_POST['id']) && $_POST['id']) {

	if ($_POST['id'] === $_SESSION['logged_user_id'] || !isValidUser($db, $_POST['id'], $_SESSION['logged_user_id']))
		echo 'Invalid user.';
	else {
		$user = getUserById($db, $_POST['id']);
		echo json_encode(array (
			'cover'			=> getFirstCoverById($db, $user['id']),
			'username'		=> $user['username'],
			'gender'		=> $user['gender'],
			'connected'		=> $user['connected'] ? 'true' : 'false',
			'age'			=> getAge($user['birth_date']),
			'place'			=> $user['city'] . ' (' . $user['location'] . ')',
			'pokemon_type'	=> strtolower($user['pokemon_type'])
		));
	}

} else
	echo 'Missing user.';

?>
