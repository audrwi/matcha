<?php

$actions = array ('block', 'report');
if (isset($_POST['username']) && $_POST['username'] !== '' && isset($_POST['action']) && $_POST['action'] !== '' && in_array($_POST['action'], $actions)) {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			if (usernameExists($db, $_POST['username'])) {
				$user = getUserByUsername($db, $_POST['username']);
				if ($user['id'] !== $_SESSION['logged_user_id']) {
					if (isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), $_POST['action']))
						echo 'You already ' . $_POST['action'] . 'ed this trainer.';
				} else
					echo 'You can\'t interact with your own account.';
			} else
				echo 'Unknown trainer.';

		} else
			echo 'Time out.';
	} else
		echo 'Something went wrong';

} else
	echo 'Missing informations.';

?>
