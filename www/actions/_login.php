<?php

if (isset($_POST['user']) && $_POST['user'] !== "" && isset($_POST['password']) && $_POST['password'] !== "") {

	if (($user = checkUser($db, array ("user" => $_POST['user'], "password" => $_POST['password']))) !== null) {

		if ($user['activated'] == 1 || $user['activated'] == 3) {

			if ($user['connected'] == 0) {
				updateActive($db, array ('id' => $user['id'], 'active' => 1));
				updateConnection($db, array ('id' => $user['id'], 'connected' => 1));
				$_SESSION['logged'] = true;
				$_SESSION['logged_user_id'] = $user['id'];
				$_SESSION['logged_status'] = 0;
			} else
				echo 'You are already connected';

		} else if ($user['activated'] == 0)
			echo 'Unactivated account';
		else if ($user['activated'] == 2)
			echo 'Deleted account';

	} else
		echo 'Wrong login or password';

} else
	echo 'Missing informations';

?>
