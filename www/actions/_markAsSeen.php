<?php

if (isset($_POST['type']) && ($_POST['type'] === 'view' || $_POST['type'] === 'like'))
	markAsSeen($db, $_SESSION['logged_user_id'], $_POST['type']);
else if (isset($_POST['type']) && $_POST['type'] === 'message' && $_POST['sender'] !== $_SESSION['logged_user_id'] || isTalkingUser($db, $_POST['sender'], $_SESSION['logged_user_id']))
	readAllMessagesFromId($db, $_POST['sender'], $_SESSION['logged_user_id']);

?>
