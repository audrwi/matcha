<?php

if (isset($_POST['image']) && $_POST['image'] !== '' && isset($_POST['width']) && $_POST['width'] !== '' && isset($_POST['height']) && $_POST['height'] !== '' &&
	isset($_POST['x']) && $_POST['x'] !== '' && isset($_POST['y']) && $_POST['y'] !== '' && isset($_POST['scale']) && $_POST['scale'] !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$mime_type = mime_content_type($_POST['image']);
			list($width, $height) = getimagesize($_POST['image']);

			if (!is_numeric($_POST['width']) || !is_numeric($_POST['height']) || !is_numeric($_POST['x']) || !is_numeric($_POST['y']) || !is_numeric($_POST['scale']))
				echo 'Wrong values';
			else if ($width < 100 || $height < 100 || $_POST['width'] < 210 || $_POST['height'] < 300)
				echo 'Image too small';
			else if ($_POST['x'] < 0 || $_POST['x'] + 210 > $_POST['width'] || $_POST['y'] < 0 || $_POST['y'] + 300 > $_POST['height'])
				echo 'Image out of frame';
			else if (!$mime_type || ($mime_type !== 'image/jpeg' && $mime_type !== 'image/png'))
				echo 'Wrong image format';
			else {
				$name = time() . generateToken() . '.jpg';
				$covers = getCovers($db, $_SESSION['logged_user_id']);
				$infos = array (
					'type'		=> $mime_type,
					'width'		=> $_POST['width'],
					'height'	=> $_POST['height'],
					'x'			=> $_POST['x'] * $_POST['scale'],
					'y'			=> $_POST['y'] * $_POST['scale']
				);
				$explodeCovers = explode(';', $covers);
				if (count($explodeCovers) === 0 || $covers === null || $covers === '') {
					echo addImage($db, array (
						'id'		=> $_SESSION['logged_user_id'],
						'image'		=> $_POST['image'],
						'name'		=> $name,
						'covers'	=> $name . '?first;',
						'infos'		=> $infos
					));
				} else if (count($explodeCovers) < 6) {
					$first = 0;
					foreach ($explodeCovers as $cover) {
						if (explode("?", $cover)[1] === "first")
							$first++;
					}
					if ($first !== 1) {
						$covers = '';
						foreach ($explodeCovers as $key => $cover) {
							$covers .= explode("?", $cover)[0];
							$covers .= ($key === 0) ? '?first;' : ';';
						}
					}
					echo addImage($db, array (
						'id'		=> $_SESSION['logged_user_id'],
						'image'		=> $_POST['image'],
						'name'		=> $name,
						'covers'	=> $covers . $name . ';',
						'infos'		=> $infos
					));
				} else
					echo 'Too many images';
			}

		} else
			echo 'Time out';
	} else
		echo 'Something went wrong';

} else
	echo 'No image found';

?>
