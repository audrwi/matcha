<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require_once('vendor/autoload.php');

if (isset($_POST['id']) && $_POST['id'] && isset($_POST['message']) && $_POST['message'] && trim($_POST['message']) !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		if ($_POST['id'] !== $_SESSION['logged_user_id'] || isTalkingUser($db, $_POST['id'], $_SESSION['logged_user_id'])) {

			if (strlen($_POST['message']) <= 994) {

				$smileys = array (':happy:', ':smile:', ':tongue:', ':sad:', ':wink:', ':grin:', ':cool:', ':angry:', ':evil:', ':shocked:', ':baffled:', ':confused:', ':neutral:', ':mustache:', ':wondering:', ':sleepy:', ':frustrated:', ':crying:');
				$replace = array ('<span class="icon-smiley-happy"></span>', '<span class="icon-smiley-smile"></span>', '<span class="icon-smiley-tongue"></span>', '<span class="icon-smiley-sad"></span>', '<span class="icon-smiley-wink"></span>', '<span class="icon-smiley-grin"></span>', '<span class="icon-smiley-cool"></span>', '<span class="icon-smiley-angry"></span>', '<span class="icon-smiley-evil"></span>', '<span class="icon-smiley-shocked"></span>', '<span class="icon-smiley-baffled"></span>', '<span class="icon-smiley-confused"></span>', '<span class="icon-smiley-neutral"></span>', '<span class="icon-smiley-mustache"></span>', '<span class="icon-smiley-wondering"></span>', '<span class="icon-smiley-sleepy"></span>', '<span class="icon-smiley-frustrated"></span>', '<span class="icon-smiley-crying"></span>');
				$new = newMessage($db, $_POST['id'], $_SESSION['logged_user_id'], htmlspecialchars(trim($_POST['message'])));
				$date = date('Y-m-d', strtotime($new['created_at']));
				$message = array (
					'sender'		=> $new['sender'] === $_SESSION['logged_user_id'] ? 'me' : 'other',
					'message'		=> str_replace($smileys, $replace, $new['message']),
					'date'			=> date('Y-m-d', time()) === $date ? 'today' : (date('Y-m-d', time() - 60 * 60 * 24) === $date ? 'yesterday' : $date),
					'hour'			=> date('H:i', strtotime($new['created_at']))
				);
				$date_prev = getDatePrevMessage($db, $new['id']);
				increasePopularity($db, array ('id' => $new['sender'], 'points' => 30));
				increasePopularity($db, array ('id' => $new['receiver'], 'points' => 50));
				$html = (!$date_prev || $date_prev !== $date) ? '<p class="date"><span>' . $message['date'] . '</span></p>' : '';
				$receiver_html = $html . '<div class="sender-other"><span class="hour">' . $message['hour'] . '</span><p class="message">' . str_replace($smileys, $replace, $new['message']) . '</p></div>';
				$html .= '<div class="sender-me"><p class="message">' . str_replace($smileys, $replace, $new['message']) . '</p><span class="hour">' . $message['hour'] . '</span></div>';
				$sender = getUserById($db, $_SESSION['logged_user_id']);
				$receiver = getUserById($db, $_POST['id']);
				$client = new Client(new Version1X('http://localhost:8000'));
				$client->initialize();
				$client->emit('user_interact', [
					'action'	=> 'message',
					'id'		=> $sender['id'],
					'token'		=> $receiver['socket'],
					'html'		=> '<p class="notif">New message from ' . ucfirst($sender['username']) . '</p>',
					'msg_html'	=> $receiver_html
				]);
				$client->close();
				echo $html;

			} else
				echo 'Too long message';

		}

	} else
		echo 'Something went wrong';

}

?>
