<?php

if (isset($_POST['email']) && $_POST['email'] !== "" && isset($_POST['token']) && $_POST['token'] !== "" &&
	isset($_POST['password']) && $_POST['password'] !== "" && isset($_POST['confirm_password']) && $_POST['confirm_password'] !== "") {

	if (isValidEmailAndToken($db, array ("email" => $_POST['email'], "token" => $_POST['token'])) && isGoodConfirmation($_POST['password'], $_POST['confirm_password'])) {

		$user = getUserByToken($db, $_POST['token']);
		updateToken($db, $user['id']);
		updatePassword($db, array ("id" => $user['id'], "password" => $_POST['password']));
		updateActive($db, array ('id' => $user['id'], 'active' => 1));

	} else
		echo 'Invalid informations';

} else
	echo 'Missing informations';

?>
