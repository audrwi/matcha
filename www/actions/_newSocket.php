<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {

	$user = getUserById($db, $_SESSION['logged_user_id']);
	$socket_token = generateToken();
	addSocketTokenToUser($db, $user['id'], $socket_token);
	echo json_encode(array (
		'id'	=> $_SESSION['logged_user_id'],
		'token'	=> $socket_token,
		'log'	=> '[' . date('D M d H:i:s Y', time()) . ']'
	));
	exit();

}

echo $_SESSION['logged'];

?>
