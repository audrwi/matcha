<?php

if (isset($_POST['tag']) && $_POST['tag'] !== '') {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$tags = getTags($db, $_SESSION['logged_user_id']);
			$explodeTags = explode(';', $tags);
			$regex = '/^[a-z0-9]{1,15}$/i';
			if (count($explodeTags) <= 10) {
				if (preg_match($regex, $_POST['tag'])) {
					$already = false;
					foreach ($explodeTags as $tag) {
						if ($tag === $_POST['tag'])
							$already = true;
					}
					if ($already === false)
						updateTag($db, array ('id' => $_SESSION['logged_user_id'], 'tags' => $tags . strtolower(trim($_POST['tag'])) . ';'));
					else
						echo 'You already have this tag';
				} else
					echo 'Invalid tag';
			} else
				echo 'Too much tags';

		} else
			echo 'Time out';

	} else
		echo 'Something went wrong';

} else
	echo 'Missing tag';

?>
