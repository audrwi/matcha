<?php

require_once('www/emailings/activate.php');

if (isset($_POST['first_name']) && $_POST['first_name'] !== "" && isset($_POST['last_name']) && $_POST['last_name'] !== "" &&
	isset($_POST['email']) && $_POST['email'] !== "" && isset($_POST['gender']) && $_POST['gender'] !== "" &&
	isset($_POST['username']) && $_POST['username'] !== "" && isset($_POST['password']) && $_POST['password'] !== "" &&
	isset($_POST['confirm_password']) && $_POST['confirm_password'] !== "" && isset($_POST['birth_date']) && $_POST['birth_date'] !== "") {

	$regex = array (
		"first_name"	=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
		"last_name"		=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
		"email"			=> '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i',
		"username"		=> '/^[a-z0-9]{5,15}$/i',
		"birth_date"	=> '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/'
	);

	if (preg_match($regex['first_name'], $_POST['first_name']) && preg_match($regex['last_name'], $_POST['last_name']) &&
		preg_match($regex['email'], $_POST['email']) && !emailExists($db, $_POST['email']) && ($_POST['gender'] === 'female' ||
		$_POST['gender'] === 'male') && preg_match($regex['username'], $_POST['username']) && !usernameExists($db, $_POST['username']) &&
		checkSecurityLevel(passwordErrors($_POST['password'])) === 'high' && preg_match($regex['birth_date'], $_POST['birth_date']) &&
		isGoodConfirmation($_POST['password'], $_POST['confirm_password']) && checkAge($_POST['birth_date']) === "") {

		$infos = array (
			"first_name"	=> $_POST['first_name'],
			"last_name"		=> $_POST['last_name'],
			"email"			=> $_POST['email'],
			"gender"		=> $_POST['gender'],
			"username"		=> $_POST['username'],
			"password"		=> $_POST['password'],
			"birth_date"	=> $_POST['birth_date']
		);
		$id = createUser($db, $infos);

		$account = getUserById($db, $id);
		$start = 'http://' . str_replace("_register.php", "", $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		$header = 'From: "PokeDate" <aboucher@student.42.fr>' . "\r\n";
		$header .= 'Reply-To: <aboucher@student.42.fr>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$content = getActivateMail($start, $_POST['username'], $account['token']);
		mail($_POST['email'], "PokeDate - Activate your account", $content, $header);

	} else
		echo 'Invalid informations';

} else
	echo 'Missing informations';

?>
