<?php

if (isset($_POST['first_name']) && $_POST['first_name'] !== '' && isset($_POST['last_name']) && $_POST['last_name'] !== '' && isset($_POST['email']) && $_POST['email'] !== '' &&
	isset($_POST['interested_by']) && $_POST['interested_by'] !== '' && isset($_POST['birth_date']) && $_POST['birth_date'] !== '' && isset($_POST['gender']) && $_POST['gender'] !== '' &&
	isset($_POST['pokemon_type']) && $_POST['pokemon_type'] !== '' && isset($_POST['location']) && $_POST['location'] !== '' && isset($_POST['description']) && $_POST['description'] !== '' &&
	isset($_POST['old_password']) && isset($_POST['password']) && isset($_POST['confirm_password'])) {

	if (isset($_POST['token']) && $_POST['token'] !== "" && isset($_SESSION['token']) && $_SESSION['token'] !== "" && $_POST['token'] === $_SESSION['token'] && isset($_SESSION['token_time'])) {

		$old_timestamp = time() - (15 * 60);
		if ($_SESSION['token_time'] >= $old_timestamp) {

			$user = getUserById($db, $_SESSION['logged_user_id']);
			$regex = array (
				"first_name"			=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
				"last_name"				=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
				"email"					=> '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i',
				"birth_date"			=> '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',
				"location"				=> '/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}/',
				"description"			=> '/^(?s:.){1,120}$/'
			);
			$infos = array ();
			$types = getAllPokemonTypes($db);

			if (preg_match($regex['first_name'], $_POST['first_name']) && preg_match($regex['last_name'], $_POST['last_name']) && preg_match($regex['email'], $_POST['email']) &&
				!emailExistsButNotMine($db, array ('id' => $_SESSION['logged_user_id'], 'email' => $_POST['email'])) && ($_POST['interested_by'] === 'male' || $_POST['interested_by'] === 'female' || $_POST['interested_by'] === 'both') &&
				preg_match($regex['birth_date'], $_POST['birth_date']) && checkAge($_POST['birth_date']) === "" && ($_POST['gender'] === 'male' || $_POST['gender'] === 'female') && in_array(ucwords($_POST['pokemon_type']), $types) &&
				preg_match($regex['location'], $_POST['location']) && preg_match($regex['description'], $_POST['description'])) {

				if (($_POST['old_password'] !== '' || $_POST['password'] !== '' || $_POST['confirm_password'] !== '') && ($_POST['old_password'] === '' || $_POST['password'] === '' || $_POST['confirm_password'] === '')) {
					echo 'Missing informations';
					exit();
				} else if ($_POST['old_password'] !== '' && $_POST['password'] !== '' && $_POST['confirm_password'] !== '') {
					if (!isGoodConfirmation($_POST['password'], $_POST['confirm_password'])) {
						echo 'Wrong password confirmation';
						exit();
					} else if (hashPassword($_POST['old_password']) !== $user['password']) {
						echo 'Wrong password';
						exit();
					}
					$array['password'] = hashPassword($_POST['password']);
				}

				foreach ($_POST as $key => $value) {
					if ($value !== $user[$key]) {
						if ($key === 'first_name' || $key === 'last_name')
							$array[$key] = ucname(trim($value));
						else if ($key === 'email')
							$array[$key] = strtolower($value);
						else if ($key === 'pokemon_type')
							$array[$key] = ucwords($value);
						else if ($key === 'location') {
							$array[$key] = substr($value, 0, 5);
							$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . substr($value, 0, 5) . '+FR&sensor=false';
							$geo = json_decode(file_get_contents($url), true);
							$geo_location = $geo['results'][0]['geometry']['location'];
							$geo_city = $geo['results'][0]['address_components'][1]['long_name'];
							$array['latitude'] = $geo_location['lat'];
							$array['longitude'] = $geo_location['lng'];
							$array['city'] = $geo_city;
						} else if ($key === 'description')
							$array[$key] = htmlspecialchars(trim($value));
						else if ($key !== 'password' && $key !== 'old_password' && $key !== 'confirm_password')
							$array[$key] = $value;
					}
				}
				updateUser($db, $_SESSION['logged_user_id'], $array);

			} else
				echo 'Invalid informations';

		} else
			echo 'Time out';
	} else
		echo 'Something went wrong';

} else
	echo 'Missing informations';

?>
