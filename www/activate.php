<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true || !isset($_GET['token'])) {
	header("Location: /");
	exit();
}

$me = getUserByToken($db, $_GET['token']);
if (!isset($me['id'])) {
	include('error.php');
	exit();
} else if ($me['activated'] == 1 || $me['activated'] == 3)
	$error = "This account is already activated.";
else if ($me['activated'] == 2)
	$error = "This account has been deleted.";
else
	updateActive($db, array ('id' => $me['id'], 'active' => 1));

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'activate';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="default">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<h1>Activate your account</h1>
		<p class="<?php if (isset($error) && $error != "") { echo 'error">' . $error; } else { ?>success">You successfully activated your account.<br>Now you can <a href="/login">login</a>.<?php } ?></p>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
