<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
} else if (isset($_SESSION['logged']) && getUserById($db, $_SESSION['logged_user_id'])['admin'] === 0) {
	header("Location: /");
	exit();
}

$token = generateToken();
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

$reported = getAllReportedUsers($db);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'admin';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php if ($_SESSION['logged'] === true) { echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; } else { echo 'default'; } ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<p id="success">You have successfully deleted this user.</p>
		<input type="hidden" name="token" value="<?php echo $token; ?>">
		<h1 class="title">Admin <span>page</span></h1>
		<div class="container">
			<?php if (empty($reported)) { ?>
			<p>No user reported as a fake account.</p>
			<?php
				} else {
					foreach ($reported as $report) {
			?>
			<article>
				<figure>
					<img src="/assets/imgs/users/<?php echo getFirstCoverById($db, $report['id']); ?>" alt="Cover">
					<span><?php echo ($report['count'] > 9) ? '9+' : $report['count']; ?></span>
				</figure>
				<div>
					<h2><span class="icon-<?php echo $report['gender']; ?>"></span><?php echo $report['username']; ?></h2>
					<p class="connected <?php echo $report['connected'] ? 'true' : 'false'; ?>"><?php echo ($report['connected']) ? 'Connected' : 'Disconnected'; ?></p>
				</div>
				<div>
					<h3>Reported by</h3>
					<p><?php
						foreach (getReportersById($db, $report['id']) as $reporter) {
							echo '<a href="/trainer-' . $reporter['username'] . '">' . $reporter['username'] . '</a>';
						}
					?></p>
				</div>
				<div id="admin_delete">
					<a href="/trainer-<?php echo $report['username']; ?>" title="View profile"><span class="icon-eye"></span></a>
					<button user-id="<?php echo $report['id']; ?>">Delete account</button>
				</div>
			</article>
			<?php } } ?>
		</div>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
