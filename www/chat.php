<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
} else if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) {
	header("Location: /");
	exit();
}

$token = generateToken();
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

$allowed = getUsersToTalk($db, $_SESSION['logged_user_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'chat';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<h1 class="title">Talk to <span>trainers</span></h1>
		<div class="container">
			<?php if (empty($allowed)) { ?>
				<p>You have to like each other's account to be able to talk.</p>
			<?php } else { ?>
				<article class="list">
					<div>
						<?php foreach ($allowed as $user) { ?><div user-id="<?php echo $user['id']; ?>">
							<figure>
								<img src="/assets/imgs/users/<?php echo getFirstCoverById($db, $user['id']); ?>" alt="Cover">
							</figure><!--
							 --><p><?php echo $user['username']; ?><span class="state <?php echo ($user['connected']) ? 'connected' : 'disconnected'; ?>"></span></p>
							<?php if ($user['count'] > 0) { ?>
								<span class="info_bubble"><?php echo $user['count']; ?></span>
							<?php } ?>
						</div><?php } ?>
					</div>
				</article><!--
				 --><article class="room">
					<div class="empty">
						<p class="tuto_msg"><span class="icon-specialarrow"></span><span class="icon-specialarrow"></span><span class="icon-specialarrow"></span> Click on a trainer to talk to him/her</p>
					</div>
					<div class="talk" user-id="">
						<a href="" title="View profile"><span class="icon-eye"></span></a>
						<div>
							<p class="nomessage">No message yet. Be the first to talk !</p>
							<div class="messages"></div>
						</div>
						<form action="" method="POST" id="send-message">
							<input type="hidden" name="token" value="<?php echo $token; ?>">
							<input type="text" name="message" value="" placeholder="WRITE YOUR MESSAGE HERE"><!--
							--><span class="icon-smiley-smile"></span><!--
							--><span class="icon-send"></span>
							<div class="smileys">
								<?php
									$smileys = array ('happy', 'smile', 'tongue', 'sad', 'wink', 'grin', 'cool', 'angry', 'evil', 'shocked', 'baffled', 'confused', 'neutral', 'mustache', 'wondering', 'sleepy', 'frustrated', 'crying');
									foreach ($smileys as $smiley) {
								?>
								<span class="icon-smiley-<?php echo $smiley; ?>"></span>
								<?php } ?>
								<div class="arrow-down"></div>
							</div>
						</form>
					</div>
				</article>
			<?php } ?>
		</div>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
