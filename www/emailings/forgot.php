<?php

function getForgotMail($url, $username, $token, $email) {
	return '<html xmlns="http://www.w3.org/1999/xhtml">
	<body bgcolor="#fbfaf8" style="margin:0px !important; padding:0px; -webkit-text-size-adjust:none;">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#fbfaf8">
		<tr>
			<td>
				<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
					<tr height="10" bgcolor="#fbfaf8">
						<td colspan="3"></td>
					</tr>
					<tr height="100" valign="center" bgcolor="#22313f">
						<td width="20"></td>
						<td width="560" align="center">
							<a href="' . $url . '" color="#ffffff" title="PokeDate" style="color:#ffffff; text-decoration:none;"><strong><font face="Arial, Helvetica, sans-serif" size="6" color="#ffffff">POKEDATE</font></strong></a>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="20"></td>
						<td width="560" align="left">
							<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#22313f">Hello ' . $username . ',<br />You just notice us that you forgot your password. You can reset it by clicking on the following link :</font>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="3"></td>
					</tr>
					<tr height="15">
						<td width="20"></td>
						<td width="560" bgcolor="#fbfbfb" style="border: 1px solid #eeeeee; border-bottom: none;" align="center"></td>
						<td width="20"></td>
					</tr>
					<tr valign="center">
						<td width="20"></td>
						<td width="560" bgcolor="#fbfbfb" style="border-left: 1px solid #eeeeee; border-right: 1px solid #eeeeee;" align="center">
							<table align="center" width="560" cellpadding="0" cellspacing="0" border="0" bgcolor="#fbfbfb">
								<tr height="10">
									<td colspan="3"></td>
								</tr>
								<tr>
									<td width="20"></td>
									<td>
										<a href="' . $url . 'forgot-' . $token . '-' . $email . '" color="#aaaaaa" title="PokeDate" style="color:#aaaaaa;"><font face="Tahoma, Arial, Helvetica, sans-serif" size="2" color="#aaaaaa">' . $url . 'forgot-' . $token . '-' . $email . '</font></a>
									</td>
									<td width="20"></td>
								</tr>
								<tr height="10">
									<td colspan="3"></td>
								</tr>
							</table>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="15">
						<td width="20"></td>
						<td width="560" bgcolor="#fbfbfb" style="border: 1px solid #eeeeee; border-top: none;" align="center"></td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="3"></td>
					</tr>
					<tr>
						<td width="20"></td>
						<td width="560" align="left">
							<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="#242729">Thanks,<br />The <a href="' . $url . '" color="#ef5350" title="PokeDate" style="color:#ef5350; text-decoration:none;"><strong><font face="Arial, Helvetica, sans-serif" size="4" color="#ef5350">PokeDate</font></strong></a> team</font>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="3"></td>
					</tr>
					<tr height="10" bgcolor="#22313f">
						<td colspan="3"></td>
					</tr>
					<tr height="10" bgcolor="#fbfaf8">
						<td colspan="3"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</body>
	</html>';
}

?>
