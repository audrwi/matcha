<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'error';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php if ($_SESSION['logged'] === true) { echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; } else { echo 'default'; } ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<h1>404 Not Found</h1>
		<img src="/assets/imgs/run.gif" alt="404 error">
		<p>The page you're looking for doesn't exist anymore.</p>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
