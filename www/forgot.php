<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
	header("Location: /");
	exit();
}

if (isset($_GET['email']) && $_GET['email'] !== "" && isset($_GET['token']) && $_GET['token'] !== "" && isValidEmailAndToken($db, array ("email" => $_GET['email'], "token" => $_GET['token'])) && getUserByToken($db, $_GET['token'])['activated'] == 3)
	$fromEmail = true;
else if (isset($_GET['email']) || isset($_GET['token'])) {
	include('error.php');
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'forgot';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="default">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<?php if (isset($fromEmail) && $fromEmail === true) { ?>
			<p id="success">You successfully changed your password.<br>You will be redirected in <span class="bold">5</span> seconds.</p>
			<h1>New password</h1>
			<form action="" method="POST" id="new_password">
				<div class="input disabled">
					<input type="email" name="email" placeholder="EMAIL ADDRESS" value="<?php echo $_GET['email']; ?>" disabled>
					<span class="icon-user"></span>
				</div>
				<div class="input password">
					<div class="security">
						<div>
							<span class="bubble"></span>
							<span class="stick"></span>
							<span class="background"></span>
						</div>
						<p></p>
					</div>
					<input type="password" name="password" placeholder="PASSWORD" value="">
					<span class="icon-user"></span>
				</div>
				<div class="input">
					<input type="password" name="confirm_password" placeholder="CONFIRM YOUR PASSWORD" value="">
					<span class="icon-lock"></span>
				</div>
				<input type="submit" value="Reset my password" disabled>
			</form>
		<?php } else { ?>
			<p id="success">An email has been sent to you to reset your password.</p>
			<h1>Forgotten password</h1>
			<form action="" method="POST" id="forgot">
				<div class="input">
					<input type="email" name="email" placeholder="EMAIL ADDRESS" value="">
					<span class="icon-user"></span>
				</div>
				<input type="submit" value="Ask for a new password" disabled>
			</form>
		<?php } ?>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
