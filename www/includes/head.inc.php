<meta charset="UTF-8">
<title><?php echo $infos['title']; ?></title>
<meta name="author" content="aboucher @ 42">
<meta name="copyright" content="&copy; Copyright 2016 | aboucher @ 42 | PokeDate">
<meta name="description" content="<?php echo $infos['description']; ?>">
<meta name="keywords" lang="fr" content="pokemon meet trainer pikachu chat date love">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="index, follow">
<meta property="og:title" content="<?php echo $infos['title']; ?>">
<meta property="og:description" content="<?php echo $infos['description']; ?>">
<meta property="og:type" content="website">
<link href="/assets/css/styles.css" rel="stylesheet">
<link rel="icon" href="/assets/imgs/favicon/favicon.ico" />
<link rel="apple-touch-icon" sizes="57x57" href="/assets/imgs/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/assets/imgs/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/assets/imgs/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/assets/imgs/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/assets/imgs/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/assets/imgs/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/assets/imgs/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/assets/imgs/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/assets/imgs/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/assets/imgs/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/imgs/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/assets/imgs/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/imgs/favicon/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/assets/imgs/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="application-name" content="Pokedate">
<script type="text/javascript" src="/assets/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="/assets/js/location_autocomplete.js"></script>
<script type="text/javascript" src="/assets/js/datepicker.js"></script>
<script type="text/javascript" src="/assets/js/autocomplete.js"></script>
<script type="text/javascript" src="/assets/js/functions.js"></script>
<script type="text/javascript" src="/assets/js/scripts.js"></script>
<script type="text/javascript" src="/assets/js/ajax_requests.js"></script>
<?php
	if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
		$socket = newSocket($db, $_SESSION['logged_user_id']);
?>
<script type="text/javascript" src="/node_modules/socket.io-client/socket.io.js"></script>
<script type="text/javascript">

	var socket	= io.connect('http://localhost:8000'),
		token	= '<?php echo $socket; ?>';

	socket.on(token, function(data) {
		if (data.action === 'view') {
			if ($('#content').hasClass('views')) {
				if ($('.container > p').length)
					$(this).remove();
				$('.container').prepend(data.view_html);
				$.ajax({
					url: '_markAsSeen.php',
					method: 'POST',
					data: {
						type:	'view'
					}
				});
			} else {
				if ($('#content').hasClass('index')) {
					if (parseInt($('#views span').text()) > 0)
						$('#views').html('<span>' + (parseInt($('#views span').text()) + 1) + '<a href="/views">See it</a>trainers viewed your profile');
					else
						$('#views span').html((parseInt($('#views span').text()) + 1) + '<a href="/views">See it</a>');
				}
				$('span.icon-eye').after(data.html).parent().addClass('new').delay(5000).queue(function() {
					$(this).children('.title').prev('.notif').remove();
				});
			}
		} else if (data.action === 'like') {
			if ($('#content').hasClass('likes')) {
				if ($('.container > p').length)
					$(this).remove();
				$('.container').prepend(data.like_html);
				$.ajax({
					url: '_markAsSeen.php',
					method: 'POST',
					data: {
						type:	'like'
					}
				});
			} else {
				if ($('#content').hasClass('index')) {
					if (parseInt($('#likes span').text()) > 0)
						$('#likes').html('<span>' + (parseInt($('#likes span').text()) + 1) + '<a href="/likes">See it</a>trainers liked your profile');
					else
						$('#likes span').html((parseInt($('#likes span').text()) + 1) + '<a href="/views">See it</a>');
				}
				if (!$('.list div[user-id=' + data.id + ']').length) {
					$('span.icon-heart').after(data.html).parent().addClass('new').delay(5000).queue(function() {
						$(this).children('.title').prev('.notif').remove();
					});
				}
			}
		} else if (data.action === 'message') {
			if ($('#content').hasClass('chat')) {
				if ($('.talk').attr('user-id') === data.id) {
					$('.empty').fadeOut();
					$('.talk .messages').append(data.msg_html);
					$('.talk').scrollTop($('.talk .messages').innerHeight());
					$.ajax({
						url: '_markAsSeen.php',
						method: 'POST',
						data: {
							type:	'message',
							sender:	data.id
						}
					});
				} else {
					if ($('.list div[user-id=' + data.id + '] span.info_bubble').length)
						$('.list div[user-id=' + data.id + '] span.info_bubble').text(parseInt($('.list div[user-id=' + data.id + '] span.info_bubble').text()) + 1);
					else
						$('.list div[user-id=' + data.id + ']').prepend('<span class="info_bubble">1</span>');
				}
			} else {
				if ($('#content').hasClass('index')) {
					if (parseInt($('#chat span').text()) > 0)
						$('#chat').html('<span>You received' + (parseInt($('#chat span').text()) + 1) + '<a href="/chat">Read it</a>messages on the chat');
					else
						$('#chat span').html((parseInt($('#chat span').text()) + 1) + '<a href="/chat">Read it</a>');
				}
				$('span.icon-talk').after(data.html).parent().addClass('new').delay(5000).queue(function() {
					$(this).children('.title').prev('.notif').remove();
				});
			}
		} else if (data.action === 'match') {
			if ($('#content').hasClass('chat') && $('.list').length && !$('.list div[user-id=' + data.id + ']').length) {
				$('.list > div').prepend(data.mtch_html);
			} else if ($('#content').hasClass('trainer') && getUrlVars()[1] === data.username) {
				$('#dislike').remove();
				$('#like').after('<a href="/chat" id="message"><button class="icon-talk"></button></a>');
			}
			if (!$('.list div[user-id=' + data.id + ']').length) {
				$('span.icon-talk').after(data.html).parent().delay(5000).queue(function() {
					$(this).children('.title').prev('.notif').remove();
					if ($('#content').hasClass('chat') && !$('.list').length)
						location.reload();
				});
			}
		}
	});

	$(window).on('beforeunload', function() {
		socket.close();
	});

</script>
<?php } ?>
