<?php
	if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
		require_once('vendor/autoload.php');
		require_once('config/server/server.php');
	}
?>
<div class="background <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) { echo 'logged'; } ?>"></div>
<p class="loader"><span></span></p>
<?php
	if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
		$themes = getAllThemes($db);
?>
	<div class="colorscheme">
		<p>Themes</p>
		<?php foreach ($themes as $theme) { ?>
		<div class="<?php echo $theme; if ($theme === getUserById($db, $_SESSION['logged_user_id'])['colorscheme']) { echo ' mine'; } ?>">
			<p><?php echo ucwords($theme) . ' theme'; ?></p>
		</div>
		<?php } ?>
	</div>
<?php } ?>
<header class="<?php if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) { echo 'logged'; } else { echo 'unlogged'; } ?>">
	<a href="/"><h1>Pokedate</h1></a>
	<nav>
		<button class="icon-menu"></button>
	<?php if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) { ?>
		<a href="/register">Register</a><!--
		--><a href="/login">Login</a>
	<?php } else { ?>
		<a href="/" class="<?php if ($page == 'index') { echo 'active'; } ?>">
			<span class="icon-home"></span>
			<p class="hidden title">Home</p>
		</a>
		<hr>
		<a href="/profile" class="<?php if ($page == 'profile') { echo 'active'; } ?>">
			<span class="icon-user"></span>
			<p class="hidden title">My profile</p>
		</a>
		<hr>
		<a href="/search" class="<?php if ($page == 'search') { echo 'active'; } ?>">
			<span class="icon-search"></span>
			<p class="hidden title">Search</p>
		</a>
		<hr>
		<a href="/likes" class="<?php
			if ($page == 'likes') {
				echo 'active';
			} else if (getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'like') > 0) {
				echo 'new';
			}
		?>">
			<span class="icon-heart"></span>
			<p class="hidden title">Likes</p>
		</a>
		<hr>
		<a href="/chat" class="nav-chat <?php
			if ($page == 'chat') { echo 'active '; }
			if (getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'message') > 0) { echo 'new'; }
		?>">
			<span class="icon-talk"></span>
			<p class="hidden title">Chat</p>
		</a>
		<hr>
		<a href="/views" class="<?php
			if ($page == 'views') {
				echo 'active';
			} else if (getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'view') > 0) {
				echo 'new';
			}
		?>">
			<span class="icon-eye"></span>
			<p class="hidden title">Views</p>
		</a>
		<?php if (getUserById($db, $_SESSION['logged_user_id'])['admin'] == 1) { ?>
		<hr>
		<a href="/admin">
			<span class="icon-shield"></span>
			<p class="hidden title">Admin page</p>
		</a>
		<?php } ?>
		<a href="" class="themes">
			<span class="icon-color"></span>
			<p class="hidden title">Colorscheme</p>
		</a>
		<div>
			<a href="/logout" class="bottom">
				<span class="icon-quit"></span>
				<p class="hidden title">Quit</p>
			</a>
		</div>
	<?php } ?>
	</nav>
