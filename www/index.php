<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'index';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) { echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; } else { echo 'default'; } ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<?php if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) { ?>
			<p><img src="/assets/imgs/pokedate.png" alt="POKEDATE"></p>
		<?php
			} else {
				$user = getUserById($db, $_SESSION['logged_user_id']);
				$likes = getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'like');
				$messages = getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'message');
				$views = getNotSeenActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'view');
		?>
			<h1 class="title">Welcome <span><?php echo $user['username']; ?></span></h1>
			<div>
				<article>
					<p>Unread news :</p>
					<p id="likes"><span><?php echo $likes; ?><a href="/likes">See it</a></span>trainer<?php if ($likes > 1) { echo 's'; } ?> liked your profile</p>
					<p id="chat">You received<span><?php echo $messages; ?><a href="/chat">Read it</a></span>message<?php if ($likes > 1) { echo 's'; } ?> on the chat</p>
					<p id="views"><span><?php echo $views; ?><a href="/views">See it</a></span>trainer<?php if ($views > 1) { echo 's'; } ?> viewed your profile</p>
					<p>We selected a few profiles for you.</p>
					<a href="/search">See selected profiles for me</a>
				</article><!--
				 --><div class="pikachu"></div>
			</div>
		<?php } ?>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
