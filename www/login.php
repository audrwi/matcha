<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
	header("Location: /");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'login';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="default">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<h1>Sign in</h1>
		<form action="" method="POST" id="login" autocomplete="off">
			<div class="input">
				<input type="text" name="user" placeholder="USERNAME OR EMAIL" value="">
				<span class="icon-user"></span>
			</div>
			<div class="input">
				<input name="password" type="password" placeholder="PASSWORD" value="">
				<span class="icon-lock"></span>
			</div>
			<input type="submit" value="Login" disabled>
		</form>
		<a href="/forgot">I forgot my password</a>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
