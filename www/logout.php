<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require_once('vendor/autoload.php');

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isset($_SESSION['logged_user_id']) && isConnected($db, $_SESSION['logged_user_id'])) {
	updateConnection($db, array ('id' => $_SESSION['logged_user_id'], 'connected' => 0));
	deleteSocket($db, $_SESSION['logged_user_id']);

	$client = new Client(new Version1X('http://localhost:8000'));
	$client->initialize();
	$client->emit('disconnection', [
		'user'	=> ucfirst(getUserById($db, $_SESSION['logged_user_id'])['username']),
		'time'	=> date('[D M d H:i:s Y] : ', time())
	]);
	$client->close();
}

$_SESSION['logged'] = false;
$_SESSION['logged_user_id'] = "";
unset($_SESSION['logged_status']);

session_destroy();
header("Location: /");

?>
