<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) {
	header("Location: /");
	exit();
}

$token = generateToken();
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

$me = getUserById($db, $_SESSION['logged_user_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'profile';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<p id="success">You have successfully updated your account.</p>
		<article>
			<h2><span>Secret</span> information</h2>
			<form action="" method="POST" id="update_hidden_informations" autocomplete="off">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
				<div class="input">
					<input type="text" name="first_name" placeholder="FIRST NAME" value="<?php echo $me['first_name']; ?>">
					<span class="icon-user"></span>
				</div>
				<div class="input">
					<input type="text" name="last_name" placeholder="LAST NAME" value="<?php echo $me['last_name']; ?>">
					<span class="icon-user"></span>
				</div>
				<div class="input">
					<input type="text" name="email" placeholder="EMAIL ADDRESS" value="<?php echo $me['email']; ?>">
					<span class="email"></span>
				</div>
				<div class="select">
					<select name="sexual_orientation" onchange="this.className=this.options[this.selectedIndex].className" class="<?php if ($me['interested_by'] == null) { echo 'default'; } else { echo 'value'; } ?>">
						<option value="" default class="default">SEXUAL ORIENTATION</option>
						<option value="<?php echo $me['gender'] === 'female' ? 'male' : 'female'; ?>" <?php if ($me['interested_by'] != null && $me['interested_by'] != 'both' && $me['interested_by'] != $me['gender']) { echo 'selected'; } ?> class="value">Heterosexual</option>
						<option value="<?php echo $me['gender']; ?>" <?php if ($me['interested_by'] == $me['gender']) { echo 'selected'; } ?> class="value">Homosexual</option>
						<option value="both" <?php if ($me['interested_by'] == 'both') { echo 'selected'; } ?> class="value">Bisexual</option>
					</select>
					<span class="icon-arrow"></span>
				</div>
				<div class="input birth">
					<input type="text" name="birth_date" placeholder="BIRTH (YYYY-MM-DD)" value="<?php echo $me['birth_date']; ?>">
					<span class="icon-calendar"></span>
				</div>
				<div class="input">
					<input type="password" name="old_password" placeholder="OLD PASSWORD" value="">
					<span class="icon-lock"></span>
				</div>
				<div class="input password">
					<div class="security">
						<div>
							<span class="bubble"></span>
							<span class="stick"></span>
							<span class="background"></span>
						</div>
						<p></p>
					</div>
					<input type="password" name="password" placeholder="NEW PASSWORD" value="">
					<span class="icon-lock"></span>
				</div>
				<div class="input">
					<input type="password" name="confirm_password" placeholder="CONFIRM YOUR PASSWORD" value="">
					<span class="icon-lock"></span>
				</div>
			</form>
		</article><!--
		 --><article>
			<h2><span>Visible</span> information</h2>
			<form action="" method="POST" id="update_visible_informations" autocomplete="off">
				<div>
					<div class="radio">
						<input type="radio" name="gender" value="female" id="gender-1" <?php if ($me['gender'] == 'female') { echo 'checked'; } ?>>
						<label for="gender-1"><span><span></span></span><p>Woman</p></label>
					</div>
					<div class="radio">
						<input type="radio" name="gender" value="male" id="gender-2" <?php if ($me['gender'] == 'male') { echo 'checked'; } ?>>
						<label for="gender-2"><span><span></span></span><p>Man</p></label>
					</div>
				</div>
				<div class="select">
					<select name="pokemon_type" onchange="this.className=this.options[this.selectedIndex].className" class="<?php if ($me['pokemon_type'] == null) { echo 'default'; } else { echo 'value'; } ?>">
						<option value="" default class="default">FAVORITE POKEMON TYPE</option>
						<?php foreach (getAllPokemonTypes($db) as $type) { ?>
						<option value="<?php echo strtolower($type); ?>" <?php if ($me['pokemon_type'] == $type) { echo 'selected'; } ?> class="value"><?php echo $type; ?></option>
						<?php } ?>
					</select>
					<span class="icon-arrow"></span>
				</div>
				<div class="input location">
					<input type="text" id="search-location" name="location" placeholder="LOCATION" value="<?php if ($me['location'] != null) { echo $me['location'] . ' (' . $me['city'] . ', France)'; } ?>" autocomplete="off">
					<span class="icon-search"></span>
					<span class="icon-location"></span>
				</div>
				<ul class="location_autocomplete"></ul>
				<div class="textarea">
					<textarea name="description" placeholder="DESCRIPTION (120 CHARS MAX)" maxlength="120"><?php if ($me['description'] != null) { echo $me['description']; } ?></textarea>
				</div>
				<div class="input interest">
					<input type="text" name="centers_of_interest" placeholder="CENTERS OF INTEREST" value="" autocomplete="off">
					<span class="icon-search"></span>
				</div>
				<div class="tags">
					<?php
						foreach (explode(";", $me['centers_of_interest']) as $tag) {
							if ($tag !== "") {
					?><span>#<?php echo $tag; ?><span class="icon-cross"></span></span><?php } } ?>
				</div>
				<div class="covers">
					<?php
						$covers = explode(";", $me['covers']);
						for ($i = 0; $i < 5; $i++) {
					?><div class="<?php if ($covers[$i] === "" || $covers[$i] === null) { echo 'empty'; } else if (explode("?", $covers[$i])[1] === "first") { echo 'first'; } ?>">
						<img src="<?php if ($covers[$i] !== "" && $covers[$i] !== null) { ?>assets/imgs/users/<?php echo explode("?", $covers[$i])[0]; } ?>" alt="Cover <?php echo $i + 1; ?>">
						<span class="icon-cross"></span>
						<span class="icon-camera"></span>
						<span>+</span>
					</div><?php } ?>
					<div class="cropper">
						<span class="icon-cross"></span>
						<figure>
							<div class="size"></div>
							<img src="">
							<div class="options">
								<span action="zoom">+</span>
								<span action="unzoom">-</span>
							</div>
						</figure><!--
						 --><article>
							<p class="error"></p>
							<h3>Add a new cover</h3>
							<input type="file" name="cover" value="" id="cover" accept="image/jpg, image/jpeg, image/png">
							<label for="cover">
								<span id="text-content">Choose an image...</span><span class="icon-upload"></span>
							</label>
							<p>Import an image and resize it to complete your profile.<br>Allowed formats : jpg, jpeg, png</p>
							<button name="new_cover" class="disabled">Add</button>
						</article>
					</div>
				</div>
			</form>
		</article>
		<div class="actions">
			<button id="update" class="<?php if(isUncompleted($db, $_SESSION['logged_user_id'])) { echo 'disabled'; } ?>">Update my informations</button><!--
		 	--><button id="delete" class="bad">Delete my account</button><!--
		 	--><a href="/trainer-<?php echo $me['username']; ?>" <?php if(isUncompleted($db, $_SESSION['logged_user_id'])) { echo 'class="disabled"'; } ?> title="View my profile"><span class="icon-eye"></span></a>
		</div>
		<p class="popularity"><span>Popularity:</span><?php echo $me['popularity']; ?> point<?php if ($me['popularity'] > 1) { echo 's'; } ?></p>
		<div class="dialog delete">
			<span class="icon-cross"></span>
			<h3>Delete my account</h3>
			<p>Are you sure ?</p>
			<button>Yes</button><!--
			 --><button>Cancel</button>
		</div>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
