<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
	header("Location: /");
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'register';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="default">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<p id="success">You have successfully created an account.<br>Check your emails to activate it.</p>
		<h1>Create an account</h1>
		<form action="" method="POST" id="register">
			<div class="input">
				<input type="text" name="first_name" placeholder="FIRST NAME" value="">
				<span class="icon-user"></span>
			</div>
			<div class="input">
				<input type="text" name="last_name" placeholder="LAST NAME" value="">
				<span class="icon-user"></span>
			</div>
			<div class="input">
				<input type="text" name="email" placeholder="EMAIL ADDRESS" value="">
				<span class="email"></span>
			</div>
			<div>
				<div class="radio">
					<input type="radio" name="gender" value="female" id="gender-1" checked>
					<label for="gender-1"><span><span></span></span><p>Woman</p></label>
				</div>
				<div class="radio">
					<input type="radio" name="gender" value="male" id="gender-2">
					<label for="gender-2"><span><span></span></span><p>Man</p></label>
				</div>
			</div>
			<div class="input">
				<input type="text" name="username" placeholder="USERNAME" value="">
				<span class="icon-user"></span>
			</div>
			<div class="input password">
				<div class="security">
					<div>
						<span class="bubble"></span>
						<span class="stick"></span>
						<span class="background"></span>
					</div>
					<p></p>
				</div>
				<input type="password" name="password" placeholder="PASSWORD" value="">
				<span class="icon-lock"></span>
			</div>
			<div class="input">
				<input type="password" name="confirm_password" placeholder="CONFIRM YOUR PASSWORD" value="">
				<span class="icon-lock"></span>
			</div>
			<div class="input birth">
				<input type="text" name="birth_date" placeholder="BIRTH (YYYY-MM-DD)" value="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
				<span class="icon-calendar"></span>
			</div>
			<input type="submit" value="Register" disabled>
		</form>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
