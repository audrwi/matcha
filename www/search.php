<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
} else if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) {
	header("Location: /");
	exit();
}

$results = getClosestsUsers($db, $_SESSION['logged_user_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'search';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<article>
			<div>
				<h1>Gonna catch 'em all !</h1>
				<h2>Here you can see the trainers' profiles we select for you</h2>
				<div class="sort">
					<h3>You also can filter by :</h3>
					<form id="filter" action="" method="POST">
						<div class="left">
							<div class="checkbox">
								<input type="checkbox" name="place" value="">
								<label for="place"><span><span class="icon-check"></span></span><p>place :</p></label>
							</div>
						</div><div class="right">
							<div class="input location">
								<input type="text" id="search-location" name="location" placeholder="LOCATION" value="" autocomplete="off">
								<span class="icon-search"></span>
							</div>
							<ul class="location_autocomplete"></ul>
						</div>
						<div class="left">
							<div class="checkbox">
								<input type="checkbox" name="tag" value="">
								<label for="tag"><span><span class="icon-check"></span></span><p>specific tag(s) :</p></label>
							</div>
						</div><div class="right">
							<div class="input interest">
								<input type="text" name="centers_of_interest" placeholder="CENTERS OF INTEREST" value="" autocomplete="off">
								<span class="icon-search"></span>
								<div class="tags"></div>
							</div>
						</div>
						<div class="left">
							<div class="checkbox">
								<input type="checkbox" name="type" value="">
								<label for="type"><span><span class="icon-check"></span></span><p>pokemon type :</p></label>
							</div>
						</div><div class="right">
							<div class="select">
								<select name="pokemon_type" onchange="this.className=this.options[this.selectedIndex].className" class="default">
									<option value="" default class="default">FAVORITE POKEMON TYPE</option>
									<?php foreach (getAllPokemonTypes($db) as $type) { ?>
									<option value="<?php echo strtolower($type); ?>" class="value"><?php echo $type; ?></option>
									<?php } ?>
								</select>
								<span class="icon-arrow"></span>
							</div>
						</div>
						<div class="left">
							<div class="checkbox">
								<input type="checkbox" name="age" value="18 - 115">
								<label for="age"><span><span class="icon-check"></span></span><p>age :</p></label>
							</div>
						</div><div class="right">
							<div id="age_slider"></div>
						</div>
						<div class="left">
							<div class="checkbox">
								<input type="checkbox" name="popularity" value="0 - 1000">
								<label for="popularity"><span><span class="icon-check"></span></span><p>popularity :</p></label>
							</div>
						</div><div class="right">
							<div id="popularity_slider"></div>
						</div>
					</form>
					<h3>Or sort by :</h3>
					<form id="sort" action="" method="POST">
						<div class="radio">
							<input type="radio" name="sort" value="place" id="sort-1" checked>
							<label for="sort-1"><span><span></span></span><p>place (the closests)</p></label>
						</div><!--
						 --><div class="radio">
							<input type="radio" name="sort" value="tags" id="sort-2">
							<label for="sort-2"><span><span></span></span><p>tags in common</p></label>
						</div><!--
						 --><div class="radio">
							<input type="radio" name="sort" value="age" id="sort-3">
							<label for="sort-3"><span><span></span></span><p>age (the closests)</p></label>
						</div><!--
						 --><div class="radio">
							<input type="radio" name="sort" value="popularity" id="sort-4">
							<label for="sort-4"><span><span></span></span><p>popularity (the bests)</p></label>
						</div>
					</form>
				</div>
				<p class="tuto_msg">Click on the <span class="mark"></span> to see the trainer <span class="icon-specialarrow"></span><span class="icon-specialarrow"></span><span class="icon-specialarrow"></span></p>
				<?php $test = getUserById($db, $_SESSION['logged_user_id']); ?>
				<div class="profile_user">
					<figure>
						<img src="/assets/imgs/users/<?php echo getFirstCoverById($db, $test['id']); ?>" alt="Cover">
					</figure><!--
					 --><div>
						<h2><?php echo $test['username']; ?><span class="icon-<?php echo $test['gender']; ?>"></span></h2>
						<p class="connected <?php echo $test['connected'] ? 'true' : 'false'; ?>"><?php echo ($test['connected']) ? 'Connected' : 'Disconnected'; ?></p>
						<p class="age"><span><?php echo getAge($test['birth_date']); ?></span> years old</p>
						<p class="place">live in <span class="location"><?php echo $test['city'] . ' (' . $test['location'] . ')'; ?></span></p>
						<p class="type <?php echo strtolower($test['pokemon_type']); ?>"></p>
						<a href="/trainer-<?php echo $test['username']; ?>">View more</a>
					</div>
				</div>
				<?php include('includes/footer.inc.php'); ?>
			</div>
		</article>
		<figure class="map">
			<?php foreach ($results as $result) { ?>
			<span user-id="<?php echo $result['id']; ?>">
				<figure>
					<img src="/assets/imgs/users/<?php echo getFirstCoverById($db, $result['id']); ?>" alt="Cover">
				</figure>
				<p><?php echo $result['username']; ?></p>
			</span>
			<?php } ?>
		</figure>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
