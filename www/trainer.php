<?php

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

require_once('vendor/autoload.php');

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
} else if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) {
	header("Location: /");
	exit();
} else if (!isset($_GET['username']) || $_GET['username'] === '' || !usernameExists($db, $_GET['username']) || isUncompleted($db, getUserByUsername($db, $_GET['username'])['id']) ||
	isInInteractionWithUser($db, array ($_SESSION['logged_user_id'], getUserByUsername($db, $_GET['username'])['id']), 'block')) {
	include('error.php');
	exit();
}

$token = generateToken();
$_SESSION['token'] = $token;
$_SESSION['token_time'] = time();

$user = getUserByUsername($db, $_GET['username']);
$mine = $user['id'] === $_SESSION['logged_user_id'];

if (!$mine && !isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), 'view')) {
	interactWithProfile($db, array ($_SESSION['logged_user_id'], $user['id']), 'view');
	increasePopularity($db, array ('id' => $user['id'], 'points' => 20));
	$user['popularity'] += 20;
	if ($user['socket'] !== NULL && $user['connected'] == 1) {
		$sender = getUserById($db, $_SESSION['logged_user_id']);
		$client = new Client(new Version1X('http://localhost:8000'));
		$client->initialize();
		$client->emit('user_interact', [
			'action'	=> 'view',
			'token'		=> $user['socket'],
			'html'		=> '<p class="notif">' . ucfirst($sender['username']) . ' just viewed your profile</p>',
			'view_html'	=> '<article><div class="unseen"></div><figure><img src="/assets/imgs/users/' . getFirstCoverById($db, $sender['id']) . '" alt="Cover"></figure><div><h2>' . $sender['username'] . '<span class="icon-' . $sender['gender'] . '"></span></h2><p class="connected ' .
							($sender['connected'] ? 'true' : 'false') . '">' . ($sender['connected'] ? 'Connected' : 'Disconnected') . '</p><p class="age"><span>' . getAge($sender['birth_date']) . '</span> years old</p><p class="place">live in <span class="location">' .
							$sender['city'] . '(' . $sender['location'] . ')</span></p><p class="type ' . strtolower($sender['pokemon_type']) . '">' . $sender['pokemon_type'] . '</p></div><a href="/trainer-' . $sender['username'] . '">View more</a></article>'
		]);
		$client->close();
	}
}

$explodeCovers = explode(';', $user['covers']);
$covers = array ();
foreach ($explodeCovers as $key => $value) {
	if ($value !== '' && isset(explode("?", $value)[1]) && explode("?", $value)[1] === 'first') {
		$covers[] = explode("?", $value)[0];
		$explodeCovers[$key] = '';
	}
}
foreach ($explodeCovers as $value) {
	if ($value !== '')
		$covers[] = $value;
}

$block = isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), 'block');
$like = isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), 'like');
$isliked = isInInteractionWithUser($db, array ($_SESSION['logged_user_id'], $user['id']), 'like');
$dislike = isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), 'dislike');
$isdisliked = isInInteractionWithUser($db, array ($_SESSION['logged_user_id'], $user['id']), 'dislike');
$report = isInInteractionWithUser($db, array ($user['id'], $_SESSION['logged_user_id']), 'report');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'trainer';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<p id="error"></p>
		<p id="success"></p>
		<input type="hidden" name="token" value="<?php echo $token; ?>">
		<h1 class="title"><?php echo $user['username']; ?><span>'s profile</span></h1>
		<div>
			<article class="slider <?php
				echo strtolower($user['pokemon_type']);
				if ($like)
					echo ' liked';
				if ($like && $isliked)
					echo ' go';
			?>">
				<div class="images">
					<?php foreach ($covers as $key => $value) { ?>
						<img src="/assets/imgs/users/<?php echo $value; ?>" alt="Cover #<?php echo $key + 1; ?>">
					<?php } ?>
				</div>
				<div class="points">
					<?php
						foreach ($covers as $key => $value) {
							echo $key === 0 ? '<div class="selected"></div>' : '<div></div>';
						}
					?>
				</div>
			</article>
			<article class="<?php echo strtolower($user['pokemon_type']); ?>">
				<div></div>
				<h2><?php echo $user['username']; ?><span class="icon-<?php echo $user['gender']; ?>"></span></h2>
				<p class="popularity">Popularity: <?php echo $user['popularity']; ?> point<?php if ($user['popularity'] > 1) { echo 's'; } ?></p>
				<p class="connected <?php echo $user['connected'] ? 'true' : 'false'; ?>"><?php echo ($user['connected']) ? 'Connected' : 'Disconnected'; ?></p>
				<?php if (!$user['connected']) { ?>
				<p class="disconnected">Last connection: <span><?php echo getDifferenceFromNow($user['last_activity']); ?></span></p>
				<?php } ?>
				<p class="whenwhere"><span class="age"><?php echo getAge($user['birth_date']); ?></span> years old - live in <span class="location"><?php echo $user['city'] . ' (' . $user['location'] . ')'; ?></span></p>
				<p>Favorite type: <span class="type <?php echo strtolower($user['pokemon_type']); ?>"><?php echo $user['pokemon_type']; ?></span></p>
				<?php if (!$mine) { ?>
					<p class="like_state"><?php
						if (!$block) {
							if ($like && $isliked)
								echo 'You liked each other\'s profile. Now you can talk.';
							else if ($like)
								echo 'You liked this trainer.';
							else if ($isliked)
								echo 'This trainer liked your profile.';
						}
					?></p>
					<p class="dislike_state"><?php
						if (!$block) {
							if ($dislike && $isdisliked)
								echo 'You disliked each other\'s profile. NO WAY !';
							else if ($dislike)
								echo 'You disliked this trainer.';
							else if ($isdisliked)
								echo 'This trainer disliked your profile.';
						}
					?></p>
					<p class="block_state"><?php if ($block) { echo 'You blocked this trainer.'; } ?></p>
					<p class="report_state"><?php if ($report) { echo 'You reported this trainer as a fake account.'; } ?></p>
					<button id="block" class="icon-stop <?php if ($block) { echo 'disabled'; } ?>"></button>
					<?php
						if (!$block) {
							if (!($like && $isliked)) {
					?>
						<button id="like" class="icon-heart <?php if ($like && !$dislike) { echo 'unlike'; } else if ($dislike) { echo 'disabled'; } ?>"></button>
						<button id="dislike" class="icon-dislike <?php if ($like || $dislike) { echo 'disabled'; } ?>"></button>
					<?php
						}
						if ($like && $isliked) {
					?>
							<button id="like" class="icon-heart unlike"></button>
							<a href="/chat" id="message"><button class="icon-talk"></button></a>
				<?php } } } ?>
			</article>
			<article class="<?php echo strtolower($user['pokemon_type']); ?>">
				<div></div>
				<h3>Centers of interest</h3>
				<p><?php
					foreach (explode(';', $user['centers_of_interest']) as $key => $value) {
						if ($value !== '')
							echo ($key === 0) ? $value : ', ' . $value;
					}
				?></p>
				<h3>Description</h3>
				<p class="description"><?php echo nl2br($user['description']); ?></p>
				<?php if (!$mine) { ?>
					<button id="report" class="<?php if ($mine || $report) { echo 'disabled'; } ?>">Report as a fake account</button>
				<?php } ?>
			</article>
		</div>
		<div class="dialog block">
			<span class="icon-cross"></span>
			<h3>Block this trainer</h3>
			<p>Are you sure ?</p>
			<button>Yes</button><!--
			 --><button>Cancel</button>
		</div>
		<div class="dialog report">
			<span class="icon-cross"></span>
			<h3>Report as a fake account</h3>
			<p>Are you sure ?</p>
			<button>Yes</button><!--
			 --><button>Cancel</button>
		</div>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
