<?php

if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (!isset($_SESSION['logged_user_id']) || !isConnected($db, $_SESSION['logged_user_id']))) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && (getUserById($db, $_SESSION['logged_user_id'])['activated'] == 0 || getUserById($db, $_SESSION['logged_user_id'])['activated'] == 2)) {
	header("Location: /logout");
	exit();
} else if (isset($_SESSION['logged']) && $_SESSION['logged'] == true && isUncompleted($db, $_SESSION['logged_user_id'])) {
	header("Location: /profile");
	exit();
} else if (!isset($_SESSION['logged']) || $_SESSION['logged'] == false) {
	header("Location: /");
	exit();
}

$views = getActionsByIdAndAction($db, $_SESSION['logged_user_id'], 'view');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		$page = 'views';
		$infos = getSEOInfos($page);
		include('includes/head.inc.php');
	?>
</head>
<body class="<?php echo getUserById($db, $_SESSION['logged_user_id'])['colorscheme']; ?>">
	<?php include('includes/header.inc.php'); ?></header><!--
	--><section id="content" class="<?php echo $page; ?>">
		<h1 class="title">Trainers <span>who viewed your profile</span></h1>
		<div class="container">
			<?php if (empty($views)) { ?>
			<p>Nobody viewed your profile yet.</p>
			<?php
				} else {
					foreach ($views as $view) {
						$user = getUserById($db, $view['sender']);
			?><article>
				<?php if ($view['seen'] == 0) { echo '<div class="unseen"></div>'; } ?>
				<figure>
					<img src="/assets/imgs/users/<?php echo getFirstCoverById($db, $user['id']); ?>" alt="Cover">
				</figure><!--
				 --><div>
					<h2><?php echo $user['username']; ?><span class="icon-<?php echo $user['gender']; ?>"></span></h2>
					<p class="connected <?php echo $user['connected'] ? 'true' : 'false'; ?>"><?php echo ($user['connected']) ? 'Connected' : 'Disconnected'; ?></p>
					<p class="age"><span><?php echo getAge($user['birth_date']); ?></span> years old</p>
					<p class="place">live in <span class="location"><?php echo $user['city'] . ' (' . $user['location'] . ')'; ?></span></p>
					<p class="type <?php echo strtolower($user['pokemon_type']); ?>"><?php echo $user['pokemon_type']; ?></p>
				</div>
				<a href="/trainer-<?php echo $user['username']; ?>">View more</a>
			</article><?php } } ?>
		</div>
		<?php include('includes/footer.inc.php'); ?>
	</section>
</body>
</html>
<?php markAsSeen($db, $_SESSION['logged_user_id'], 'view'); ?>
